# FASE DE DESEÑO

## Casos de uso

1. Realizar un puzzle
2. Analizar posiciones de ajedrez
3. Salir de la aplicación

![Casos de uso](img/use_case.png)

## Diseño de la interfaz de usuario

### **Web**
  - _Landing page_ -> mostrará un resumen del perfil del usuario (en el prototipo actual, como no hay la funcionalidad de perfiles de usuario, se mostrará solo un mensaje de bienvenida)
  - Página de puzzles
![web1](img/web1.png)
  - Análisis de aperturas / posiciones
![web2](img/web3.png)

### **Android**
  - Pantalla inicial: muestra varios botones para acceder al resto de pantallas.
  - Pantalla de puzzles

![android2](img/android2.png)
  - Análisis de aperturas / posiciones


![android1](img/android1.png)


## Base de Datos

### Modelo Entidad / Relación

![E/R](img/e_r.png)

### Modelo Relacional

#### Leyenda:
- ***Claves primarias en negrita y subrayadas***
- **FK = Foreign Key**
- *Claves candidatas en cursiva*

![Relacional](img/relacional.png)


## Diagrama de componentes software que constitúyen el producto y de despliegue

![Componentes](img/components.png)
