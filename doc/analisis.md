# Análisis preliminar
## Objetivos de aprendizaje
Se pretende profundizar en el desarrollo de aplicaciones multiplataforma, en este caso profundizando en el desarrollo para Android, usando Java y también aprendiendo a usar el nuevo lenguaje recomendado para desarrollar en Android (Kotlin).

Por otro lado se pretende aprender tecnologías para el desarrollo web, y observar cómo el desarrollo de esta parte se diferencia con el de Android.

Un desafío adicional va a ser saber adaptar el diseño de la interfaz e interacción de usuario para cada entorno (por un lado el entorno táctil de móbiles y por otro un entorno para ordenadores).

## Alcance
Los principales requisitos serán:
- Desarrollo y despliegue de una página web (chesslearn.io) donde el usuario pueda acceder a dos pantallas:
    - La pantalla para aprendizaje de teoría
      - Se llamará Análisis de aperturas, y aquí el usuario dispone de un tablero de ajedrez en su posición inicial, y podrá realizar movimientos simulando una partida. En cada posición se mostrará información relevante que permitirá su estudio:
        - Nombre de la apertura (si la posición está registrada en la Enciclopedia de aperturas de ajedrez).
        - Popularidad de la posición.
        - El porcentaje de victorias del jugador de las blancas y negras para esta posición.
        - Una valoración de la posición (para qué jugador es ventajosa) usando información precalculada por un engine de ajedrez. (Se usará el popular Stockfish, engine de código abierto).
      - Toda la información estará guardada en una base de datos en el back-end de la web.
    - La pantalla para practicar tácticas mediante puzzles
      - Constará de diversos puzzles guardados en el back-end y expuestos mediante una API REST. Los puzzles permitirán al usuario practicar su reconocimiento de las diversas tácticas en el ajedrez: Ataques dobles, Piezas sobrecargadas, Ataques a la descubierta, Jaque a la descubierta, Eliminar el defensor…
- Desarrollo de aplicación nativa para Android
  - Tendrá el mismo contenido que la página web, pero implementado de forma nativa para dispositivos Android.
- Desarrollo de una aplicación interna que rellene la base de datos con información extraída de partidas de ajedrez reales.
  - Se descargará un archivo que contiene la información de todas las partidas de ajedrez realizadas en Febrero de 2021 en la página lichess.org. Se trata de información disponible de forma pública desde https://database.lichess.org/
  - La aplicación analizará este archivo, procesará las partidas usando el motor de análisis Stockfish y calculará diversas estadísticas. Toda esta información se guardará en la base de datos que se guardará en el back-end de la web, y a la cual se accederá en la pantalla de Análisis de aperturas de la aplicación y la web.

