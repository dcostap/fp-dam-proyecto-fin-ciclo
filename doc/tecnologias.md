# Tecnologías
Este documento describe las diversas tecnologías y librerías que han sido utilizadas. Todas las tecnologías usadas contienen licencias permisivas y permiten la comercialización del software.

## Front-end

Se utilizó HTML y Sass (el cual se compila a CSS mediante un comando).

Se utilizó el framework [Bulma](https://bulma.io/) para estilizar la página con componentes y estilos ya predefinidos. Se eligió este framework por diversas razones:
- Nos permite tener un diseño web moderno y que se adapta a diversas resoluciones de forma automática.
- Solo modifica CSS y no incluye javascript, lo cual hace esta dependencia más ligera y nos da el control para animar o programar la interfaz de la forma que queramos.
- Utiliza [Sass](https://sass-lang.com/) para modificar los estilos. Esto nos permite de forma muy sencilla personalizar / sobreescribir los estilos que Bulma trae por defecto. De esta forma se pudo implemntar el botón en la esquina superior derecha que permite activar el modo oscuro de la web.

[Animate.css](https://animate.style/): se utiliza para añadir animaciones a la interfaz.

Para la programación en front-end se utilizó Javascript, ayudándonos de diversas librerías:
- [chess.js](https://github.com/jhlywa/chess.js). Implementa las reglas de Ajedrez, usado de forma local en el front-end para detectar movimientos inválidos cuando el usuario interactúa con el tablero de ajedrez.
- [jquery](https://jquery.com/). Una de las librerías más populares, que, entre otras cosas, facilita la forma de acceder / crear / modificar elementos HTML desde javascript.
- [chessboard.js](https://chessboardjs.com/). Nos permite fácilmente añadir un tablero de ajedrez a una página HTML. Incorpora ya programada la interacción con el usuario (arrastre de piezas).

## Back-end

- Python & [Flask](https://flask.palletsprojects.com/)
  - Flask sirve los _requests_ de API REST.
    - Accediendo a la url `./get_opening/<fen_base64>`, el servidor devuelve un JSON con información sobre la posición indicada. (Todas las posiciones usan la notación [FEN](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation), y el string que se indica en la URL debe estar codificado en BASE64)
    - Accediendo a la url `./new_puzzle` devuelve un JSON con toda la información sobre un puzzle que el servidor ha extraído de forma aleatoria.
  - Para simplificar se utiliza (¿temporalmente?) el mismo back-end framework Flask para servir las páginas y recursos estáticos. En un futuro se migraría a nginx.
- Gunicorn. Es el *Web Server Gateway Interface* (WSGI) utilizado para el despliegue.
- [chess](https://python-chess.readthedocs.io/en/v1.6.1/). Implementación de las reglas de ajedrez para realizar la validación de todos los movimientos que son enviados al servidor.

## Android
- Android SDK
- Java
- Kotlin
- [libgdx](https://libgdx.com/) (librería de gráficos con la cual implementé el dibujado del tablero de ajedrez y las piezas, y el manejo de la interacción con el usuario)
- [chesslib](https://github.com/bhlangonijr/chesslib/) (implementación de las reglas de ajedrez)