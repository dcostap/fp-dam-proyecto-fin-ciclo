# Descripción del proyecto

El proyecto consiste en desarrollar una aplicación web (chesslearn.io) enfocada en el aprendizaje y mejora del ajedrez. En esta web cada usuario podrá practicar las dos partes fundamentales del aprendizaje del ajedrez: la teoría y las tácticas de juego.
Se tratará de desarrollar la web, de forma que sea funcional, y desarrollar además una aplicación nativa para android.

# Contextualización

## Destinatario
El proyecto va destinado a cualquier jugador de ajedrez que quiera mejorar su juego. Se trata de aprovechar el reciente crecimiento en popularidad del juego, proporcionando una aplicación que se especialice en el área del aprendizaje.

## Competidores
No se tratará de competir con las grandes aplicaciones y webs de ajedrez (chess.com, lichess.org), las cuales se enfocan en la competición con otras personas. Estas aplicaciones suelen disponer de su propia zona de aprendizaje enfocada en la realización de puzzles; chesslearn.io en cambio se enfocará en ejercicios especializados y que permiten mejorar el juego de forma más entretenida y cómoda para el usuario.

## Comercialización
La aplicación se podrá comercializar ofreciendo un servicio de suscripción, de la misma forma que otras aplicaciones de ajedrez (como por ejemplo chess.com). Este servicio de suscripción ofrecerá más ejercicios y opciones para el usuario.

Debido a la reciente explosión en popularidad del juego en plataformas online, esta aplicación se considera una buena oportunidad de negocio.
