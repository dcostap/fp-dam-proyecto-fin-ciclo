# ESTUDIO DE NECESIDADES Y MODELO DE NEGOCIO

## Justificación de las necesidades detectadas que cubre el sistema a desarrollar

La aplicación realizada trata de ser una forma fácil de mejorar en el ajedrez. El ajedrez es un juego fácil de aprender pero difícil de dominar, por lo cual cualquier ayuda al aprendizaje es muy valiosa para cualquier jugador que quiera mejorar su juego.

Para cumplir este objetivo, esta aplicación se especializará en las áreas más relevantes en cuanto al aprendizaje. La versión final de la aplicación incluirá cuentas de usuario y un registro de estadísticas, ofreciendo una experiencia de aperendizaje adaptada a cada usuario.

Se prevee que un prototipo del proyecto con toda la funcionalidad e infraestructura básica se podrá realizar en un mes.

## Posibilidades de comercialización (viabilidad, competidores…)

### Viabilidad.

**Viabilidad técnica**: Para establecer el prototipo básico será necesario un solo desarrollador con conocimientos básicos de *backend* y *frontent*. No se preveen dificultades técnicas, ya que tanto para la web como la aplicación en Android se utilizarán tecnologías estándar y muy populares.

**Viabilidad económica**: El proyecto supondrá una pequeña inversión al inicio mientras no se establezcan los sistemas de monetización. Se enfocará en acumular usuarios y publicitar el servicio. En cualquier caso gastos como los costes de servidor se adaptarán y escalarán con la popularidad del servicio, por lo que el riesgo es pequeño.

### Competencia.

Las páginas de ajedrez más populares (lichess.org, chess.com) no se enfocan en el aprendizaje, por lo que es posible publicitarse de forma fácil como una aplicación de aprendizaje "acompañante", y no una alternativa a esas páginas.

## Ideas para su comercialización

### Promoción.

Se promocionará la aplicación en las páginas más populares de ajedrez. Será una forma fácil de realizar una promoción eficiente, poniendo los anuncios a la vista de los usuarios interesados por el juego.

### Modelo de negocio.

La aplicación proporcionará las características más básicas de forma gratuita, y se añadirán varios servicios de suscripción que desbloquearán nuevas funcionalidades. Se trata de una forma de monetización muy típica y usada en páginas como chess.com.



