from cairosvg import svg2png
import os

for file in os.listdir("."):
    if file.endswith(".svg"):
        svg2png(
            open(file, 'rb').read(),
            write_to=open(os.path.splitext(file)[0] + ".png", 'wb'),
            scale = 0.25,
            )