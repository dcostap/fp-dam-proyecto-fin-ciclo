
                CREATE TABLE IF NOT EXISTS posicion_tablero (
                ID_posicion PRIMARY KEY AUTOINCREMENT, FEN TEXT NOT NULL, descripcion TEXT,
                SAN TEXT NOT NULL, es_error BOOLEAN, es_apertura_inicial BOOLEAN);

            
INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq', null, '1. e4 c5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/8/3pP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1ppppp/2n5/2pnP3/8/N1P2N2/PP1P1PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. c3 Nf6 3. e5 Nd5 4. Nf3 Nc6 5. Na3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pp1p/3p2p1/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2n3p1/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2N3p1/8/4P3/8/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Nxc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2n3p1/8/2PNP3/8/PP3PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1pppbp/2n3p1/8/2PNP3/4B3/PP3PPP/RN1QKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. c4 Bg7 6. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp1pppbp/2n3p1/8/2PNP1n1/2N1B3/PP3PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. c4 Bg7 6. Be3 Nf6 7. Nc3 Ng4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pp1p/3p1np1/8/2PQP3/2N5/PP3PPP/R1B1KB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. c4 Nf6 6. Nc3 Nxd4 7. Qxd4 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp1pppbp/2n2np1/8/2BNP3/2N1B3/PPP2PPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Nc3 Bg7 6. Be3 Nf6 7. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2n3p1/8/3NP3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1pppbp/2n3p1/8/2PNP3/8/PP3PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. c4 Bg7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np2pn/8/2P1P3/8/PPN1BPPP/RNBQK2R w KQkq', null, '1. Nf3 c5 2. c4 g6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc2 Bg7 6. e4 d6 7. Be2 Nh6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk1nr/pp1ppp1p/6pb/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 g6 3. c4 Bh6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/2P5/PP1P1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/pp2pppp/8/2pq4/8/2P5/PP1P1PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. c3 d5 3. exd5 Qxd5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r3kbnr/pp2pppp/2n5/3q4/3P2b1/5N2/PP3PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. c3 d5 3. exd5 Qxd5 4. d4 cxd4 5. cxd4 Nc6 6. Nf3 Bg4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r3kbnr/pp2pppp/8/8/3n4/2N2P2/PP3P1P/R1B1KB1R w KQkq', null, '1. e4 c5 2. c3 d5 3. exd5 Qxd5 4. d4 cxd4 5. cxd4 Nc6 6. Nf3 Bg4 7. Nc3 Bxf3 8. gxf3 Qxd4 9. Qxd4 Nxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1k1nr/pp3ppp/2n5/3qp3/1b1P4/2N2N2/PP2BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. c3 d5 3. exd5 Qxd5 4. d4 Nc6 5. Nf3 cxd4 6. cxd4 e5 7. Nc3 Bb4 8. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rn2kb1r/pp2pppp/5n2/2pq4/3P2b1/2P2N2/PP3PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. c3 d5 3. exd5 Qxd5 4. d4 Nf6 5. Nf3 Bg4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2n1p3/2pnP3/3P4/2P2N2/PP3PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. c3 Nf6 4. e5 Nd5 5. d4 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1ppppp/8/3nP3/3p4/2P5/PP3PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. c3 Nf6 3. e5 Nd5 4. d4 cxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1ppppp/1nn5/2p1P3/8/1BP2N2/PP1P1PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. c3 Nf6 3. e5 Nd5 4. Nf3 Nc6 5. Bc4 Nb6 6. Bb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kb1r/ppqppp1p/1nn5/4P1p1/2p5/2P2N2/PPBPQPPP/RNB1K2R w KQkq', null, '1. e4 c5 2. c3 Nf6 3. e5 Nd5 4. Nf3 Nc6 5. Bc4 Nb6 6. Bb3 c4 7. Bc2 Qc7 8. Qe2 g5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P1Q1/8/PPPP1PPP/RNB1KBNR b KQkq', null, '1. e4 c5 2. Qg4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp2pppp/2np4/2p5/4PP2/2PP4/PP4PP/RNBQKBNR b KQkq', null, '1. e4 c5 2. d3 Nc6 3. c3 d6 4. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2np1n2/4p3/3NP3/2N5/PPP1BPPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Be2 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2np1n2/4p3/4P3/1NN5/PPP1BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 e5 7. Nb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2Np1n2/4p3/4P3/2N5/PPP1BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 e5 7. Nxc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/2B1P3/8/PPPP1PPP/RNBQK1NR b KQkq', null, '1. e4 c5 2. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/7N/PPPP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nh3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1pp1pp/8/2p2p2/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 f5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1pppp1/7p/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 h6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qkbnr/pp1bpp1p/2np2p1/1Bp1P3/8/5N2/PPPPQPPP/RNB2RK1 b kq', null, '1. e4 c5 2. Nf3 d6 3. Bb5+ Nc6 4. O-O Bd7 5. Qe2 g6 6. e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r3kb1r/pp1qpppp/2np1n2/2p5/3PP3/2P2N2/PP3PPP/RNBQ1RK1 b kq', null, '1. e4 c5 2. Nf3 d6 3. Bb5+ Bd7 4. Bxd7+ Qxd7 5. O-O Nc6 6. c3 Nf6 7. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rn1qkbnr/pp1bpppp/3p4/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. Bb5+ Bd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qkb1r/1p2pppp/p2p1n2/2p3B1/3Pb3/2P2N2/PP3PPP/RN1QR1K1 b kq', null, '1. e4 c5 2. Nf3 d6 3. Bb5+ Nc6 4. O-O Bd7 5. c3 Nf6 6. Re1 a6 7. Bxc6 Bxc6 8. d4 Bxe4 9. Bg5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. Bb5+', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rn2kbnr/pp1qpppp/3p4/2p5/2P1P3/5N2/PP1P1PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. Bb5+ Bd7 4. Bxd7+ Qxd7 5. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/8/3QP3/5N2/PPP2PPP/RNB1KB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Qxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/pp1qpppp/2np4/1B6/3QP3/5N2/PPP2PPP/RNB1K2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Qxd4 Nc6 5. Bb5 Qd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2N5/PPP1BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kb1r/pp2pppp/1qnp1n2/8/2BNP3/2N5/PPP2PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4 Qb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pp1p/3p1np1/8/3QP3/2N5/PPP1BPPP/R1B1K2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Be2 Nxd4 7. Qxd4 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/2N5/PPPP1PPP/R1BQKBNR w KQkq', null, '1. e4 c5 2. Nc3 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/2N3P1/PPPP1P1P/R1BQKBNR b KQkq', null, '1. e4 c5 2. Nc3 e6 3. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2n3p1/2p5/4P3/2N3P1/PPPP1P1P/R1BQKBNR w KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1pppbp/2n3p1/2p5/4P3/2N3P1/PPPP1PBP/R1BQK1NR w KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp2ppbp/2np2p1/2p5/4P3/2NP2P1/PPP2PBP/R1BQK1NR w KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp3ppp/2np4/2p1p1b1/2B1P3/2NP4/PPPN1PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. Nc3 e5 4. Bc4 Be7 5. d3 d6 6. Nd2 Bg5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/2N5/PPPP1PPP/R1BQKBNR b KQkq', null, '1. e4 c5 2. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp2ppbp/2np2p1/2p5/4PP2/2NP2P1/PPP3BP/R1BQK1NR b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6 6. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp2ppbp/2np2p1/2p5/4P3/2NPB1P1/PPP2PBP/R2QK1NR b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6 6. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp3pbp/2np2p1/2p1p3/4PP2/2NP2P1/PPP3BP/R1BQK1NR w KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6 6. f4 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp3pbp/2np2p1/2p1p3/4P3/2NP2P1/PPP1NPBP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6 6. Nge2 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2npbp/2np2p1/2p1p3/4PP2/2NP2PN/PPP3BP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 d6 6. f4 e5 7. Nh3 Nge7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/4P3/2N5/PPPPNPPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. Nge2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/4P3/2N3P1/PPPP1P1P/R1BQKBNR b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/4P1P1/2N5/PPPP1P1P/R1BQKBNR b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp3ppp/4p3/2pp4/4P3/2N3P1/PPPP1P1P/R1BQKBNR w KQkq', null, '1. e4 c5 2. Nc3 e6 3. g3 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/4P3/2N5/PPPP1PPP/R1BQKBNR w KQkq', null, '1. e4 c5 2. Nc3 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1p1pbp/4p1p1/2p5/3nP3/3PB1P1/PPP1NPBP/R2QK1NR b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. g3 g6 4. Bg2 Bg7 5. d3 e6 6. Be3 Nd4 7. Nce2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1ppppp/2n2n2/8/2B1P3/8/PPP2PPP/RNBQK1NR b KQkq', null, '1. e4 c5 2. d4 cxd4 3. Qxd4 Nc6 4. Qd1 Nf6 5. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kb1r/pp2pppp/2np4/q2P4/8/2P2N2/P3BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. c3 Nf6 4. Be2 Nc6 5. d4 cxd4 6. cxd4 Nxe4 7. d5 Qa5+ 8. Nc3 Nxc3 9. bxc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np4/8/3Pn3/5N2/PP2BPPP/RNBQK2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. c3 Nf6 4. Be2 Nc6 5. d4 cxd4 6. cxd4 Nxe4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1pp1pp/7n/2p2P2/8/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 f5 3. exf5 Nh6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N5/PPP1BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPP1BPPP/R2QK2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. Be2 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPP1BPPP/R2Q1RK1 b kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2 Bg7 7. O-O Nc6 8. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np1np1/8/4P3/1NN1B3/PPP1BPPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. Be2 Nc6 8. Nb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/1p2ppbp/2np1np1/p7/4P3/1NN1B3/PPP1BPPP/R2Q1RK1 w -', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Nc3 Bg7 6. Be3 Nf6 7. Be2 O-O 8. O-O d6 9. Nb3 a5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPPQBPPP/R4RK1 b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2 Bg7 7. O-O O-O 8. Be3 Nc6 9. Qd2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp2ppbp/3pbnp1/n7/4PP2/1NN1B3/PPP1B1PP/R2Q1RK1 w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2 Bg7 7. O-O O-O 8. Be3 Nc6 9. Nb3 Be6 10. f4 Na5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/4P3/1NN1B3/PPP1BPPP/R2Q1RK1 b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2 Bg7 7. O-O O-O 8. Be3 Nc6 9. Nb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r4rk1/pp2ppbp/3p1np1/q4P2/4P1P1/2N1B3/PPP1Q2P/R4RK1 b -', null, '1. e4 c5 2. Nc3 d6 3. f4 Nc6 4. Nf3 g6 5. d4 cxd4 6. Nxd4 Bg7 7. Be3 Nf6 8. Be2 O-O 9. Nb3 Be6 10. O-O Na5 11. f5 Bc4 12. Nxa5 Bxe2 13. Qxe2 Qxa5 14. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1q2rk1/pp2ppbp/2npbnp1/8/4PP2/1NN1B3/PPP1B1PP/R2Q1RK1 w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2 Bg7 7. O-O O-O 8. Be3 Nc6 9. Nb3 Be6 10. f4 Qc8', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pp1p/3p1np1/8/3NPP2/2N5/PPP3PP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1npp1p/3p1np1/8/3NPP2/2N5/PPP3PP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. f4 Nbd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np1np1/8/2BNP3/2N1B3/PPP2PPP/R2QK2R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Nc3 Bg7 6. Be3 Nf6 7. Bc4 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbq1rk1/pp2ppbp/3p1np1/8/3NP3/2N1BP2/PPP3PP/R2QKB1R w KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1BP2/PPPQ2PP/R3KB1R w KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp1bppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/R3K2R w KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4 Bd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp1bppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/2KR3R b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4 Bd7 10. O-O-O', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1BP2/PPP3PP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp2ppbp/3pbnp1/8/2BBP3/2N2P2/PPPQ2PP/R3K2R w KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 Nc6 8. Qd2 O-O 9. Bc4 Nxd4 10. Bxd4 Be6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/pp2ppbp/3p1np1/8/3NP3/2N1BP2/PPP3PP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/R3K2R b KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1BP2/PPPQ2PP/2KR1B1R b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 Nc6 8. Qd2 O-O 9. O-O-O', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    '2rq1rk1/pp1bppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/2KR3R w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4 Bd7 10. O-O-O Rc8', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/3NP1P1/2N1BP2/PPPQ3P/R3KB1R b KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1r3k1/pp1bppb1/2np1np1/q6p/3NP2P/1BN1BP2/PPPQ2P1/2KR3R w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4 Bd7 10. O-O-O Qa5 11. h4 Rfc8 12. Bb3 h5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp1nppbp/2np2p1/8/2BNP3/2N1BP2/PPPQ2PP/R3K2R w KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 Nc6 8. Qd2 O-O 9. Bc4 Nd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N1B3/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/pp2ppbp/3p1np1/8/3NP3/2N1B3/PPP1BPPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPPQBPPP/R3K2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 g6 7. Be3 Bg7 8. Qd2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp2ppbp/5np1/n2p1P2/4P3/1NNPB3/PP4PP/R2Q1RK1 w -', null, '1. e4 c5 2. Nf3 g6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 Bg7 7. O-O O-O 8. Be3 Nc6 9. Nb3 Be6 10. f4 Na5 11. f5 Bc4 12. Bd3 Bxd3 13. cxd3 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPPQBPPP/2KR3R b -', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 g6 7. Be3 Bg7 8. Qd2 O-O 9. O-O-O', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp2ppbp/3p1np1/n4P2/2b1P3/1NNBB3/PPP3PP/R2Q1RK1 b -', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Nc3 Bg7 6. Be3 Nf6 7. Be2 O-O 8. Nb3 d6 9. O-O Be6 10. f4 Na5 11. f5 Bc4 12. Bd3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b2rk1/pp2ppbp/1qnp1np1/4P3/3N1P2/2N1B3/PPP1B1PP/R2Q1RK1 b -', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 g6 5. Be3 Bg7 6. Be2 Nf6 7. Nc3 O-O 8. O-O d6 9. f4 Qb6 10. e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1r3k1/pp1bppbp/2np1np1/q7/3NP2P/1BN1BP2/PPPQ2P1/2KR3R b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4 Bd7 10. h4 Qa5 11. O-O-O Rfc8 12. Bb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/1p2ppbp/2np1np1/p7/2BNP3/2N1BP2/PPPQ2PP/R3K2R w KQ', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be3 Bg7 7. f3 O-O 8. Qd2 Nc6 9. Bc4 a5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 e6 2. d4 c5 3. Nf3 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/ppqppppp/2n5/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Qc7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2n1pn2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp1p1ppp/2n1pn2/1Nb5/4P3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e6 6. Ndb5 Bc5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2N1pn2/8/4P3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Nxc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1p1ppp/2n1p3/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1pp2p/6p1/2p2p2/3PP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 g6 3. d4 f5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1p1ppp/4pn2/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/8/3pP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/1P3N2/P1PP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. b3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kb1r/pp1p1ppp/1q2pn2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Qb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp2ppp1/2np4/2p4p/2P1P3/2N3P1/PP1P1P1P/R1BQKBNR w KQkq', null, '1. e4 c5 2. c4 d6 3. Nc3 Nc6 4. g3 h5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/pp1ppppp/1qn5/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Qb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/4PP2/2N5/PPPP2PP/R1BQKBNR b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1p1pbp/2n1p1p1/2p2P2/2B1P3/2N2N2/PPPP2PP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nc3 Nc6 3. f4 g6 4. Nf3 Bg7 5. Bc4 e6 6. f5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P1P1/8/PPPP1P1P/RNBQKBNR b KQkq', null, '1. e4 c5 2. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/3pPP2/8/PPP3PP/RNBQKBNR b KQkq', null, '1. e4 c5 2. d4 cxd4 3. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppp1p/6p1/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppp1p/6p1/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 g6 3. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk1nr/pp1pppbp/6p1/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 g6 3. d4 Bg7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1k1nr/pp1ppp1p/6p1/2P5/4P3/2q2N2/P1P2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 g6 3. d4 Bg7 4. dxc5 Qa5+ 5. Nc3 Bxc3+ 6. bxc3 Qxc3+', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/8/2p1p3/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp3ppp/2np4/1N2p3/4P3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 e5 5. Nb5 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/1p1p1ppp/p1n1pn2/8/1bPNP3/2NB4/PP3PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. c4 Nf6 6. Nc3 Bb4 7. Bd3 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1p1p/p3p1p1/8/2PNP3/8/PP3PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. c4 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/8/2PNP3/8/PP3PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/8/3NP3/3B4/PPP2PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Bd3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk1nr/1p1p1ppp/p3p3/2b5/3NP3/3B4/PPP2PPP/RNBQK2R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Bd3 Bc5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1p1p/p3p1p1/8/3NP3/3B4/PPP2PPP/RNBQK2R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Bd3 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/3p1ppp/p3p3/1p6/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Nc3 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1k1nr/3p1ppp/pq2p3/1pb5/3NP1Q1/2NBB3/PPP2PPP/R3K2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Nc3 b5 6. Bd3 Qb6 7. Be3 Bc5 8. Qg4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/3p1ppp/p3p3/1p6/3NP3/2N3P1/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. Nc3 a6 4. g3 b5 5. d4 cxd4 6. Nxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/3p1ppp/pq2p3/1p6/4P3/2NB1N2/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Nc3 b5 6. Bd3 Qb6 7. Nf3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3p1p/p2ppnp1/8/2PNP3/3B4/PP3PPP/RNBQ1RK1 w kq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. Bd3 Nf6 6. O-O d6 7. c4 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/1p1p1ppp/p1n1pn2/8/1bPNP3/2N5/PPB2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 a6 5. c4 Nf6 6. Nc3 Bb4 7. Bd3 Nc6 8. Bc2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/p2ppppp/1p6/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 b6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPPNPPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Ne2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPPKPPP/RNBQ1BNR b kq', null, '1. e4 c5 2. Ke2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/p3pppp/3p4/1pp5/4P3/5NP1/PPPP1P1P/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. g3 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/N7/PPPP1PPP/R1BQKBNR b KQkq', null, '1. e4 c5 2. Na3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/8/2BpP3/N7/PPP2PPP/R1BQK1NR b KQkq', null, '1. e4 c5 2. Na3 Nc6 3. d4 cxd4 4. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rn1qkb1r/pp1bpppp/3p1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Bd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/pp1p1ppp/1q2p3/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Qb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/6P1/PPPP1P1P/RNBQKBNR b KQkq', null, '1. e4 c5 2. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2n2n2/4p3/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qkb1r/1p3ppp/p1npbn2/4p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Ndb5 d6 7. Bg5 a6 8. Na3 Be6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2N2n2/4p3/4P3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Nxc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2n2n2/4p3/4P3/2N2N2/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Nf3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2n2n2/4p3/4P3/1NN5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Nb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Ndb5 d6 7. Bg5 a6 8. Na3 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/5p1p/p1np4/1p1Npp2/4P3/N7/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Ndb5 d6 7. Bg5 a6 8. Na3 b5 9. Bxf6 gxf6 10. Nd5 f5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/5ppp/p1np1n2/1p1Np1B1/4P3/N7/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Ndb5 d6 7. Bg5 a6 8. Na3 b5 9. Nd5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/5pbp/p1np1p2/1p1Np3/4P3/N7/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Ndb5 d6 7. Bg5 a6 8. Na3 b5 9. Bxf6 gxf6 10. Nd5 Bg7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/5p1p/p1np4/1B1Npp2/4P3/N7/PPP2PPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Ndb5 d6 7. Bg5 a6 8. Na3 b5 9. Bxf6 gxf6 10. Nd5 f5 11. Bxb5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1p1ppp/2n5/4p3/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/p3pp1p/2pp1np1/4P3/2B5/2N5/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4 g6 7. Nxc6 bxc6 8. e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/2N5/PPP2PPP/R1BQKBNR w KQkq', null, '1. e4 c5 2. Nc3 e6 3. d4 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4PP2/8/PPPP2PP/RNBQKBNR b KQkq', null, '1. e4 c5 2. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pppp/5n2/2pP4/5P2/8/PPPP2PP/RNBQKBNR w KQkq', null, '1. e4 c5 2. f4 d5 3. exd5 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/P7/1PPP1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. a3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pppp/3p1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pppp/3p1n2/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pppp/3p4/2P5/4n3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 Nf6 4. dxc5 Nxe4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pppp/3p1n2/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/pp1ppppp/8/q1p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Qa5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/3pP3/5N2/PPP2PPP/RNBQKB1R b KQkq', null, '1. d4 c5 2. e4 cxd4 3. Nf3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/8/4p3/3pP3/2P2N2/PP3PPP/RNBQKB1R b KQkq', null, '1. d4 c5 2. e4 cxd4 3. Nf3 e5 4. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P2P/8/PPPP1PP1/RNBQKBNR b KQkq', null, '1. e4 c5 2. h4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/P3P3/8/1PPP1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. a4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/1p2bppp/p2ppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Be7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N4P/PPP2PP1/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. h3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NPP2/2N5/PPP3PP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1k2r/1pq1bpp1/p2ppn1p/8/3NPP1B/2N2Q2/PPP3PP/R3KB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Be7 8. Qf3 h6 9. Bh4 Qc7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP1P1/2N5/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N1B3/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p4/8/3NP1n1/2N1B3/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be3 Ng4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N5/PPP2PPP/R1BQKBR1 b Qkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Rg1', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/1p2bp2/p2ppn1p/6p1/3NPP1B/2N2Q2/PPP3PP/R3KB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Be7 8. Qf3 h6 9. Bh4 g5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1k2r/1pqnbppp/p2ppn2/6B1/3NPP2/2N2Q2/PPP3PP/2KR1B1R w kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Be7 8. Qf3 Qc7 9. O-O-O Nbd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/1p3ppp/p1nppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N5/PPP1BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rn1qk2r/1p2bppp/p2pbn2/4p3/4P3/1NN5/PPP1BPPP/R1BQ1RK1 w kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be2 e5 7. Nb3 Be7 8. O-O Be6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbq1rk1/1p2bppp/p2p1n2/4p3/4P3/1NN5/PPP1BPPP/R1BQ1RK1 w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be2 e5 7. Nb3 Be7 8. O-O O-O', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kb1r/1p3ppp/p2ppn2/6B1/3NPP2/q1N5/P1PQ2PP/1R2KB1R w Kkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Qb6 8. Qd2 Qxb2 9. Rb1 Qa3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kb1r/1p3ppp/pq1ppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Qb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/5ppp/p2ppn2/1p4B1/3NPP2/2N5/PPP3PP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kb1r/2q2ppp/p3pn2/1p2P1B1/3N4/2N5/PPP1Q1PP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 b5 8. e5 dxe5 9. fxe5 Qc7 10. Qe2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/1p1n1ppp/p2ppn2/8/3NP3/2N5/PPP1BPPP/R1BQ1RK1 w kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be2 e6 7. O-O Nbd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1k2r/1pq1bppp/p2ppn2/6B1/3NPP2/2N2Q2/PPP3PP/R3KB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 e6 7. f4 Be7 8. Qf3 Qc7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r3kb1r/1b3ppp/p2ppn2/qpn1P1B1/3N4/1BN5/PPPQ1PPP/2KRR3 b kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bg5 Nbd7 7. Bc4 Qa5 8. Qd2 e6 9. O-O-O b5 10. Bb3 Bb7 11. Rhe1 Nc5 12. e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p2p1n2/8/2BNP3/2N5/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp2pppp/2n5/3p4/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1ppppp/5n2/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1ppppp/5n2/2p1P3/8/5N2/PPPP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nf6 3. e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1ppppp/5n2/2p5/4P3/2N2N2/PPPP1PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nf6 3. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1ppppp/8/2p1P3/8/2n2N2/PPPP1PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nf6 3. e5 Nd5 4. Nc3 Nxc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1p1ppp/2n5/2ppP3/3P4/5N2/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nf6 3. e5 Nd5 4. Nc3 e6 5. Nxd5 exd5 6. d4 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2n3p1/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1p1pbp/2n3p1/1Bp1p3/3PP3/2P2N2/PP3PPP/RNBQ1RK1 b kq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 g6 4. O-O Bg7 5. c3 e5 6. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp1pppbp/2n2np1/1Bp5/3PP3/2P2N2/PP3PPP/RNBQ1RK1 b kq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 g6 4. O-O Bg7 5. c3 Nf6 6. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp1pppbp/2n2np1/1Bp5/Q3P3/2P2N2/PP1P1PPP/RNB2RK1 b kq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 g6 4. O-O Bg7 5. c3 Nf6 6. Qa4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1p1pbp/2n3p1/1Bp1p3/1P2P3/5N2/P1PP1PPP/RNBQR1K1 b kq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 g6 4. O-O Bg7 5. Re1 e5 6. b4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/8/nBp5/1P2P3/5N2/P1PP1PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 Na5 4. b4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/5N2/PPPPBPPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/1P3N2/P1PP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. b3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/1p1p1ppp/p1n5/4p3/2PNP3/8/PP3PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 a6 5. c4 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. c4 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p2pppp/p2p4/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. c4 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/8/2BpP3/5N2/PPP2PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. d4 cxd4 4. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. d4 cxd4 4. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p7/4p3/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. d4 cxd4 4. Nxd4 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/8/3QP3/5N2/PPP2PPP/RNB1KB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. d4 cxd4 4. Qxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/3P1N2/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. d3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/5NP1/PPPP1P1P/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p1ppppp/p4n2/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. c3 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p2pppp/p4n2/2pP4/8/2P2N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. c3 d5 4. exd5 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/3ppppp/p7/1pp5/4P3/2P2N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. c3 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p2pppp/p2p4/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 a6 3. c3 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/1P2P3/5N2/P1PP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. b4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1ppppp/p7/2p5/4P3/2N2N2/PPPP1PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 a6 3. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/8/3pP3/5N2/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppppp/2n5/8/3NP3/8/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp1ppppp/2n2n2/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1p1ppp/2n1p3/8/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/1p1p1ppp/p1n1p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp1p1ppp/2nNpn2/8/1b2P3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Ndb5 Bb4 7. Nd6+', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/ppqp1ppp/2n1p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/ppqp1ppp/2n1p3/8/3NP3/2N1B3/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/1pqp1ppp/p1n1p3/8/3NP3/2N1B3/PPP1BPPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Be3 a6 7. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/1pqp1ppp/p1n1p3/8/3NP3/2N1BP2/PPP3PP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Qc7 5. Nc3 e6 6. Be3 a6 7. f3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/1pqp1ppp/p1n1p3/8/3NP3/2N1B3/PPPQ1PPP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Be3 a6 7. Qd2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rqb1kbnr/1p1p1ppp/pBn1p3/1N6/4P3/2N5/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Ndb5 Qb8 7. Be3 a6 8. Bb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/1p3ppp/p1n1pn2/3p4/2P1P3/N1N5/PP3PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nb5 d6 6. c4 Nf6 7. N1c3 a6 8. Na3 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/4bppp/ppnppn2/8/2P1P3/N1N5/PP2BPPP/R1BQ1RK1 w -', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nb5 d6 6. c4 Nf6 7. N1c3 a6 8. Na3 Be7 9. Be2 O-O 10. O-O b6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1p1ppp/2n1p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1p1ppp/2n1p3/1N6/4P3/8/PPP2PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nb5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/1p1pnppp/p1n1p3/8/3NP3/2N5/PPP1BPPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 a6 6. Be2 Nge7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk1nr/pp1p1ppp/4p3/2b5/3NP3/8/PPP2PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Bc5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/pp1p1ppp/4pn2/8/1b1NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Bb4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/pp1p1ppp/5n2/4p3/1b1NP3/2NB4/PPP2PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Bb4 6. Bd3 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqk2r/pp1p1ppp/4pn2/4P3/1b1N4/2N5/PPP2PPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Bb4 6. e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/p2ppppp/8/1pp5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp2pppp/3p1n2/8/3NP3/5P2/PPP3PP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. f3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3p1n2/1B2p3/3NP3/5P2/PPP3PP/RNBQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. f3 e5 6. Bb5+', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/ppqppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq', null, '1. e4 c5 2. Nf3 Qc7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/6B1/3NP3/2NQ4/PPP2PPP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2bppp/2nppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 Be7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2bppp/2nppn2/6B1/3NPP2/2N5/PPPQ2PP/2KR1B1R b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 Be7 8. O-O-O O-O 9. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2bppp/2np1n2/4p1B1/3NPP2/2N5/PPPQ2PP/2KR1B1R w -', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 e6 7. Qd2 Be7 8. O-O-O O-O 9. f4 e5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2bppp/3ppn2/6B1/3QPP2/2N5/PPP3PP/2KR1B1R b -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 Be7 8. O-O-O O-O 9. f4 Nxd4 10. Qxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/4bppp/p2ppn2/1p4B1/3QPP2/2N5/PPP3PP/2KR1B1R w kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 Be7 8. O-O-O Nxd4 9. Qxd4 a6 10. f4 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pp1p/2np1np1/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2Nppn2/6B1/4P3/2N5/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Nxc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kb1r/pp3ppp/1qnppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 Qb6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qkb1r/pp1bpppp/2np1n2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 Bd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qkb1r/pp1bpppp/2np1n2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 Bd7 7. Qd2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qkb1r/1p1b1ppp/p1nppn2/6B1/3NP3/2N5/PPPQ1PPP/2KR1B1R w kq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 e6 7. Qd2 a6 8. O-O-O Bd7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qk2r/1p1bbppp/p1nppn2/6B1/3NPP2/2N5/PPPQ2PP/2KR1B1R w kq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 e6 7. Qd2 a6 8. O-O-O Bd7 9. f4 Be7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/1p3ppp/p1nppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2qk2r/3bbppp/p1nppB2/1p6/4PP2/2N2N2/PPPQ2PP/2KR1B1R b kq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 e6 7. Qd2 a6 8. O-O-O Bd7 9. f4 Be7 10. Nf3 b5 11. Bxf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/6B1/4P3/1NN5/PPP2PPP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bg5 e6 7. Nb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/1B4B1/3NP3/2N5/PPP2PPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Bb5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2bppp/3ppn2/6B1/3nPP2/2N5/PPPQ2PP/2KR1B1R w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bg5 e6 7. Qd2 Be7 8. O-O-O O-O 9. f4 Nxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. Bb5 Nb8', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/8/3NP3/2N5/PPP1BPPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/8/3NP3/2N5/PPP1BPPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be2 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kb1r/1pq2ppp/p2ppn2/8/3NP3/2N5/PPP1BPPP/R1BQ1RK1 w kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be2 e6 7. O-O Qc7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kb1r/1pq2ppp/p1nppn2/8/3NPP2/2N5/PPP1B1PP/R1BQ1RK1 w kq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. f4 e6 7. Be2 Qc7 8. O-O Nc6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kb1r/1pq2ppp/p1nppn2/8/3NPP2/2N1B3/PPP1B1PP/R2Q1RK1 b kq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Be2 a6 7. O-O Nf6 8. Be3 d6 9. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1k2r/1pq1bppp/p1nppn2/8/P2NPP2/2N5/1PP1B1PP/R1BQ1R1K b kq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Be2 a6 7. O-O Nf6 8. Kh1 Be7 9. f4 d6 10. a4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/8/3NP1P1/2N1B3/PPP2P1P/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be3 e6 7. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3p1p/p2p1np1/4pNP1/4P3/2N1B3/PPP2P1P/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be3 e6 7. g4 e5 8. Nf5 g6 9. g5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/8/3NP3/2N1BP2/PPP3PP/R2QKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be3 e6 7. f3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/8/3NP3/2N1B3/PPPQ1PPP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Be3 e6 7. Qd2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. g3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/8/3NP1P1/2N5/PPP2P1P/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. g4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/8/3NPP2/2N5/PPP3PP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/8/3NP3/2N5/PPP1BPPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 d6 6. Be2 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2bppp/2nppn2/8/3NPP2/2N1B3/PPP1B1PP/R2Q1RK1 b -', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 Be7 7. O-O O-O 8. f4 Nc6 9. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r2q1rk1/pp1bbppp/2nppn2/8/4PP2/1NN1B3/PPP1B1PP/R2Q1RK1 b -', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Be2 Be7 7. O-O O-O 8. f4 Nc6 9. Be3 Bd7 10. Nb3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2bppp/2nppn2/8/3NPP2/2N1BQ2/PPP3PP/R3KB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. f4 Nc6 7. Be3 Be7 8. Qf3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/1B6/3NP3/2N5/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bb5+', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b2rk1/1pq1bppp/p1nppn2/8/3NPP2/2N1B3/PPP1B1PP/R3QRK1 w -', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nc6 5. Nc3 Qc7 6. Be2 a6 7. O-O Nf6 8. Be3 Be7 9. f4 d6 10. Qe1 O-O', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/3PP3/8/PPP2PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. d4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/3pP3/2P5/PP3PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p3ppp/p2ppn2/8/2B1P3/2N2N2/PP3PPP/R1BQ1RK1 w kq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 d6 5. Nf3 e6 6. Bc4 Nf6 7. O-O a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/1p2pppp/p1np1n2/8/2B1P3/2N2N2/PP3PPP/R1BQ1RK1 w kq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 d6 6. Bc4 a6 7. O-O Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp1ppp1p/2n3p1/8/4P3/2N2N2/PP3PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 g6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/1p1p1ppp/p3p3/8/4P3/2N2N2/PP3PPP/R1BQKB1R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 e6 5. Nf3 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1k1nr/1pqp1ppp/p1nbp3/8/2B1P3/2N2N2/PP2QPPP/R1B2RK1 w kq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 e6 6. Bc4 Qc7 7. Qe2 a6 8. O-O Bd6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1p1ppp/2n1p3/2b5/2B1P3/2N2N2/PP3PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 e6 6. Bc4 Bc5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/3p1ppp/p1n1p3/1pb5/4P3/1BN2N2/PP3PPP/R1BQ1RK1 w kq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 e6 6. Bc4 a6 7. O-O b5 8. Bb3 Bc5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/1p1p1ppp/p1n1p3/8/2B1P3/2N2N2/PP3PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 e6 6. Bc4 a6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk1nr/pp1p1ppp/2n1p3/8/1bB1P3/2N2N2/PP3PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 e6 6. Bc4 Bb4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/pp3ppp/2npp3/8/2B1P3/2N2N2/PP3PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Nf3 d6 6. Bc4 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkbnr/5ppp/p1npp3/1p6/2B1P3/2N2N2/PP2QPPP/R1B2RK1 w kq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Bc4 e6 6. Nf3 d6 7. O-O a6 8. Qe2 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/1p1pnppp/p3p3/8/2B1P3/2N2N2/PP3PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 e6 5. Bc4 a6 6. Nf3 Ne7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp1ppppp/5n2/8/3pP3/2P5/PP3PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 Nf6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/8/4p3/3pP3/2P5/PP3PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. c3 e5 3. d4 cxd4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/2P1P3/3p4/PP3PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 d3 4. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/4P3/2Pp4/PP3PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 d3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/8/3p4/3pP3/2P5/PP3PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 d5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/pp1ppppp/8/q7/3pP3/2P5/PP3PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 Qa5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. c3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    '2bqkbnr/r4ppp/p1npp3/1p6/4P3/1BN2N2/PP2QPPP/R1B2RK1 w k', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nxc3 Nc6 5. Bc4 e6 6. Nf3 d6 7. O-O a6 8. Qe2 b5 9. Bb3 Ra7', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/4P3/2p2N2/PP3PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. d4 cxd4 3. c3 dxc3 4. Nf3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/4P3/1P6/P1PP1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. b3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/p2ppppp/1p6/2p5/4P3/1P6/P1PP1PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. b3 b6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3ppn2/8/2BNP3/2N5/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/5ppp/p2ppn2/1p6/3NP3/1BN5/PPP2PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. Bc4 e6 7. Bb3 b5', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/8/2BNP3/2N5/PPP2PPP/R1BQK2R w KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4 e6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp3ppp/2nppn2/8/2BNP3/2N1B3/PPP2PPP/R2QK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4 e6 7. Be3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/p3bppp/1p1ppn2/n7/3NPP2/1BN1B3/PPP3PP/R2Q1RK1 w -', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 Nc6 6. Bc4 e6 7. Be3 Be7 8. Bb3 O-O 9. O-O Na5 10. f4 b6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bq1rk1/pp2bppp/2nppn2/8/3NPP2/1BN1B3/PPP3PP/R2QK2R b KQ', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4 e6 7. Bb3 Be7 8. Be3 O-O 9. f4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/8/2BNP3/2N5/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/8/4P3/2N5/PPP1NPPP/R1BQKB1R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Nde2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/2P1P3/8/PP1P1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqk2r/pp2bppp/2nppn2/8/2BNP3/2N1B3/PPP1QPPP/R3K2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bc4 e6 7. Be3 Be7 8. Qe2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkb1r/pp3ppp/3p1n2/1B2p3/3NP3/2N5/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 e5 6. Bb5+', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/2p5/1P2P3/8/P1PP1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. b4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1p1ppp/4p3/2p5/1P2P3/5N2/P1PP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 e6 3. b4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/1p2P3/8/PBPP1PPP/RN1QKBNR b KQkq', null, '1. e4 c5 2. b4 cxb4 3. Bb2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/4P3/p7/2PP1PPP/RNBQKBNR w KQkq', null, '1. e4 c5 2. b4 cxb4 3. a3 bxa3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp2pppp/3p4/2p5/1P2P3/5N2/P1PP1PPP/RNBQKB1R b KQkq', null, '1. e4 c5 2. Nf3 d6 3. b4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/pp2pppp/8/3q4/1p6/P7/1BPP1PPP/RN1QKBNR b KQkq', null, '1. e4 c5 2. b4 cxb4 3. a3 d5 4. exd5 Qxd5 5. Bb2', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/1p2P3/P7/2PP1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. b4 cxb4 3. a3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnb1kbnr/pp3ppp/4q3/4p3/1pP5/P2B1N2/3P1PPP/RNBQK2R b KQkq', null, '1. e4 c5 2. b4 cxb4 3. a3 d5 4. exd5 Qxd5 5. Nf3 e5 6. c4 Qe6 7. Bd3', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1b1kbnr/pp3ppp/2n1q3/4p3/1pP5/P4N2/1B1P1PPP/RN1QKB1R w KQkq', null, '1. e4 c5 2. b4 cxb4 3. a3 d5 4. exd5 Qxd5 5. Nf3 e5 6. Bb2 Nc6 7. c4 Qe6', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'rnbqkbnr/pp1ppppp/8/8/1pP1P3/8/P2P1PPP/RNBQKBNR b KQkq', null, '1. e4 c5 2. b4 cxb4 3. c4', false, false);

INSERT INTO posicion_tablero(
    FEN, descripcion, SAN, es_error, es_apertura_inicial)
VALUES (
    'r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2NB4/PPP2PPP/R1BQK2R b KQkq', null, '1. e4 c5 2. Nf3 Nc6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 d6 6. Bd3', false, false);
