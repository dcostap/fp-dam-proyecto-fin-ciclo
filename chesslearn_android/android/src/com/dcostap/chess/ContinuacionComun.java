package com.dcostap.chess;

import androidx.annotation.NonNull;

import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.move.Move;

import java.util.List;

public class ContinuacionComun {
    private int id;
    private String moveString;
    private Move move;
    private int timesPlayed;
    private int blackWins;
    private int whiteWins;
    private int draws;
    private float evaluation;

    private float popularity;
    private float rating;

    public ContinuacionComun(int id, String moveString, int timesPlayed, int blackWins, int whiteWins, int draws, float evaluation, Move move) {
        this.id = id;
        this.moveString = moveString;
        this.timesPlayed = timesPlayed;
        this.blackWins = blackWins;
        this.whiteWins = whiteWins;
        this.draws = draws;
        this.evaluation = evaluation;
        this.move = move;
    }

    public Move getMove() {
        return move;
    }

    /** Calcula la popularidad del movimiento comparándolo con el resto de movimientos en la posición, y le asigna una valoración */
    public void calculatePopularityAndRating(Side sideToMove, List<ContinuacionComun> all) {
        int total = all.stream().mapToInt(c -> c.timesPlayed).sum();
        popularity = timesPlayed / (float) total;

        rating = evaluation / 3.5f;

        // asignamos un peso para el cálculo de valoración que deriva de la comparación de victorias de blanco vs negro
        // esto se debe a que las estadísticas pierden mucho valor y pueden volverse incorrectas si no hay el número suficiente
        // de partidas analizadas en el que se jugó la posición
        float weight = Math.min(timesPlayed / 1300f, 1f);
        if (whiteWins > 0 && blackWins > 0)
            rating += (((sideToMove == Side.BLACK ? whiteWins : blackWins) / (float) (sideToMove == Side.BLACK ? blackWins : whiteWins)) - 1) * weight;
    }

    public int getId() {
        return id;
    }

    public String getMoveString() {
        return moveString;
    }

    public int getTimesPlayed() {
        return timesPlayed;
    }

    public int getBlackWins() {
        return blackWins;
    }

    public int getWhiteWins() {
        return whiteWins;
    }

    public int getDraws() {
        return draws;
    }

    public float getEvaluation() {
        return evaluation;
    }

    public float getPopularity() {
        return popularity;
    }

    public float getRating() {
        return rating;
    }

    @NonNull
    @Override
    public String toString() {
        return moveString;
    }
}
