package com.dcostap.chess;

import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.core.app.NavUtils;
import androidx.fragment.app.FragmentActivity;

import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.dcostap.chess.utils.Utils;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.move.Move;
import com.github.bhlangonijr.chesslib.move.MoveConversionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;

public class ActivityGame extends FragmentActivity implements AndroidFragmentApplication.Callbacks, ChessGameListener {

    private static final Integer SOUND_MOVE_ID = 212;
    private static final Integer SOUND_CAPTURE_ID = 123;
    private GameFragment fragment;

    static ChessGame gdxGame;
    private LinearLayout linearLayoutBottom;

    private boolean isBottomPaneHome = true;
    private HashMap<Integer, ArrayList<ContinuacionComun>> continuacionComunMap = new HashMap<>();

    public void setBottomPaneHome(boolean bottomPaneHome) {
        isBottomPaneHome = bottomPaneHome;

        ImageButton btHome = findViewById(R.id.bt_home);
        ImageButton btList = findViewById(R.id.bt_list);

        if (isBottomPaneHome) {
            btList.setBackgroundColor(getResources().getColor(R.color.darker));
            btHome.setBackgroundColor(getResources().getColor(R.color.less_dark));
        } else {
            btHome.setBackgroundColor(getResources().getColor(R.color.darker));
            btList.setBackgroundColor(getResources().getColor(R.color.less_dark));
        }

        updateBottomView();
    }

    private void updateBottomView() {
        runOnUiThread(() -> linearLayoutBottom.removeAllViews());

        if (gdxGame == null) return;

        ArrayList<ContinuacionComun> currentContinuaciones = continuacionComunMap.get(gdxGame.getMovesMade());

        if (!isBottomPaneHome) {
            // segunda pestaña, en la que se muestran las posibles continuaciones de una posición
            if (currentContinuaciones == null) return;
            runOnUiThread(() -> {
                linearLayoutBottom.removeAllViews();
                ListView lv = new ListView(getApplicationContext());

                lv.setAdapter(new CustomListViewAdapter(currentContinuaciones, this, gdxGame.board.getSideToMove() == Side.WHITE));

                // remove invalid moves (this is a patch for a bug where invalid continuations may sometimes appear...)
                currentContinuaciones.removeIf(c -> {
                    boolean exception = false;

                    try {
                        Utils.decodeSan(gdxGame.board, c.getMoveString(), gdxGame.board.getSideToMove());
                    } catch (MoveConversionException e) {
                        exception = true;
                        e.printStackTrace();
                    }
                    return exception;
                });

                // al clickar en una posición, se dibuja una flecha en el tablero que muestra de forma visual dicha posición
                lv.setOnItemClickListener((parent, view, position, id) -> {
                    if (position == 0) return;

                    try {
                        Move move = Utils.decodeSan(gdxGame.board, currentContinuaciones.get(position - 1).getMoveString(), gdxGame.board.getSideToMove());
                        gdxGame.highlightMove = move;
                    } catch (MoveConversionException e) {
                        e.printStackTrace();
                    }
                });
                linearLayoutBottom.addView(lv);
            });
        } else {
            Move lastMove = gdxGame.getLastMove();
            runOnUiThread(() -> {
                View v = LayoutInflater.from(getApplicationContext()).inflate(
                        R.layout.layout_home_tab,
                        linearLayoutBottom,
                        false);

                LinearLayout ll1 = v.findViewById(R.id.home_tab_ll1);
                LinearLayout ll2 = v.findViewById(R.id.home_tab_ll2);
                LinearLayout ll3 = v.findViewById(R.id.home_tab_ll3);

                if (currentOpening != null) {
                    TextView tv = new TextView(getApplicationContext());
                    tv.setText(currentOpening);
                    tv.setTextColor(getResources().getColor(R.color.white));

                    ImageView iv = new ImageView(getApplicationContext());
                    iv.setImageDrawable(getApplicationContext().getDrawable(R.drawable.book));
                    int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getApplicationContext().getResources().getDisplayMetrics());
                    ll1.addView(iv, new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));
                    ll1.addView(tv);

                    ll1.setOnLongClickListener(v1 -> {
                        Toast.makeText(getApplicationContext(), getText(R.string.opening_name), Toast.LENGTH_SHORT).show();
                        return false;
                    });
                }

                ArrayList<ContinuacionComun> lastContinuaciones = null;
                System.out.println("move counter: " + gdxGame.getMovesMade());
                if (gdxGame.getMovesMade() > 0) {
                    lastContinuaciones = continuacionComunMap.get(gdxGame.getMovesMade() - 1);
                }
                if (lastMove == null || lastContinuaciones == null) {
                    TextView tv = new TextView(getApplicationContext());
                    tv.setText(R.string.try_to_find_good_move);
                    tv.setTextSize(18);
                    tv.setTextColor(getResources().getColor(R.color.white));
                    ll2.addView(tv);
                } else {
                    ImageView imageView = null;

                    TextView tv = new TextView(getApplicationContext());
                    tv.setTextColor(getResources().getColor(R.color.white));
                    ImageView ratingImageView = null;
                    String ratingText = "";

                    ContinuacionComun cc = lastContinuaciones.stream()
                            .filter(c -> c.getMove().equals(lastMove))
                            .findFirst().orElse(null);

                    if (cc != null) {
                        int ratingPercent = AndroidUtils.getRatingPercent(cc);
                        Drawable ratingDrawable = getApplicationContext().getDrawable(R.drawable.cancel);
                        ratingText = cc.getMoveString();

                        if (ratingPercent >= 60) {
                            ratingDrawable = getApplicationContext().getDrawable(R.drawable.star);
                            ratingText += " " + getString(R.string.was_great_move);
                        } else if (ratingPercent >= 35) {
                            ratingText += " " + getString(R.string.was_good_move);
                            ratingDrawable = getApplicationContext().getDrawable(R.drawable.checkmark);
                        } else {
                            ratingText += " " + getString(R.string.was_bad_move);
                        }

                        ratingImageView = new ImageView(getApplicationContext());
                        ratingImageView.setImageDrawable(ratingDrawable);

                        imageView = new ImageView(getApplicationContext());
                        imageView.setImageDrawable(
                                AndroidUtils.drawableForMove(getApplicationContext(), gdxGame.board.getSideToMove() != Side.WHITE, cc.getMoveString())
                        );
                    } else {
                        ratingText = getString(R.string.no_data);
                    }

                    tv.setText(ratingText + "  ");
                    tv.setTextSize(15);

                    int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getApplicationContext().getResources().getDisplayMetrics());
                    if (imageView != null)
                        ll2.addView(imageView, new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));
                    ll2.addView(tv);
                    if (ratingImageView != null)
                        ll2.addView(ratingImageView, new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));
                }

                linearLayoutBottom.addView(v);
            });
        }
    }

    private SoundPool soundPool = new SoundPool(2, AudioManager.STREAM_NOTIFICATION, 100);
    private HashMap<Integer, Integer> soundPoolMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        soundPoolMap.put(SOUND_MOVE_ID, soundPool.load(getApplicationContext(), R.raw.move, 1));
        soundPoolMap.put(SOUND_CAPTURE_ID, soundPool.load(getApplicationContext(), R.raw.capture, 1));

        setContentView(R.layout.layout_chess_game);

        FrameLayout fl = findViewById(R.id.frame_gdx);
        fl.getLayoutParams().height = fl.getWidth();

        setActionBar(findViewById(R.id.toolbar_game));

        getActionBar().setDisplayHomeAsUpEnabled(true);

        fragment = new GameFragment(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_gdx, fragment)
                .commit();
        fragment.listener = this;

        linearLayoutBottom = findViewById(R.id.ll_bottom);

        ImageButton btHome = findViewById(R.id.bt_home);
        try {
            btHome.setImageBitmap(BitmapFactory.decodeStream(getAssets().open("info.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ImageButton btList = findViewById(R.id.bt_list);
        try {
            btList.setImageBitmap(BitmapFactory.decodeStream(getAssets().open("menu.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ImageButton btLeftArrow = findViewById(R.id.bt_arrows_left);
        try {
            btLeftArrow.setImageBitmap(BitmapFactory.decodeStream(getAssets().open("arrows_left.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ImageButton btRightArrow = findViewById(R.id.bt_arrows_right);
        try {
            btRightArrow.setImageBitmap(BitmapFactory.decodeStream(getAssets().open("arrows_right.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        btHome.setOnClickListener(v -> {
            if (!isBottomPaneHome) {
                setBottomPaneHome(true);
            }
        });

        btList.setOnClickListener(v -> {
            if (isBottomPaneHome) {
                setBottomPaneHome(false);
            }
        });

        btLeftArrow.setOnClickListener(v -> {
            continuacionComunMap.remove(gdxGame.getMovesMade());
            gdxGame.undoMove();
        });

        btRightArrow.setOnClickListener(v -> {
            gdxGame.redoMove();
        });

        setBottomPaneHome(true);
    }

    String currentOpening = null;

    @Override
    public void onNewBoardPosition(String fen) {
        if (!continuacionComunMap.containsKey(gdxGame.getMovesMade()))
            continuacionComunMap.put(gdxGame.getMovesMade(), retrieveContinuacionComunList(fen));

        updateOpening(fen);

        runOnUiThread(() -> {
            Toolbar toolbar = findViewById(R.id.toolbar_game);
            toolbar.setTitle(gdxGame.board.getSideToMove() == Side.BLACK ? getString(R.string.black_to_move) : getString(R.string.white_to_move));
        });

        updateBottomView();
    }

    @Override
    public void onMove(String san) {
        soundPool.play(soundPoolMap.get(SOUND_MOVE_ID), 1, 1, 1, 0, 0);
    }

    @Override
    public void onCapture(String san) {
        soundPool.play(soundPoolMap.get(SOUND_CAPTURE_ID), 1, 1, 1, 0, 0);
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    // caché temporal de las posiciones a las que accedimos para no acceder a la URL más veces de las necesarias y sentir el delay
    private HashMap<String, JSONObject> cachedPositionJsonObjects = new HashMap<>();

    private JSONObject getPositionInfoFromWeb(String fen) throws IOException, JSONException {
//        if (cachedPositionJsonObjects.containsKey(fen))
//            return cachedPositionJsonObjects.get(fen);

        String fenBase64 = Base64.getEncoder().encodeToString(fen.getBytes());
        System.out.println("https://dcostap-chess.herokuapp.com/get_opening/" + fenBase64);
        JSONObject jsonObject = readJsonFromUrl("https://dcostap-chess.herokuapp.com/get_opening/" + fenBase64);
        cachedPositionJsonObjects.put(fen, jsonObject);
        return jsonObject;
    }

    public ArrayList<ContinuacionComun> retrieveContinuacionComunList(String fen) {
        ArrayList<ContinuacionComun> continuacionComunList = new ArrayList<>();

        try {
            System.out.println("fen: " + fen);
            JSONObject jsonObject = getPositionInfoFromWeb(fen);

            JSONArray continuations = jsonObject.getJSONArray("common_continuations");
            for (int i = 0; i < continuations.length(); i++) {
                JSONObject cont = (JSONObject) continuations.get(i);
                continuacionComunList.add(new ContinuacionComun(
                        cont.getInt("ID_cont"),
                        cont.getString("move"),
                        cont.getInt("times_played"),
                        cont.getInt("black_wins"),
                        cont.getInt("white_wins"),
                        cont.getInt("draws"),
                        cont.getInt("move_rating"),
                        Utils.decodeSan(gdxGame.board, cont.getString("move"), gdxGame.board.getSideToMove())
                ));
            }

            continuacionComunList.forEach(c -> c.calculatePopularityAndRating(gdxGame.board.getSideToMove(), continuacionComunList));
            continuacionComunList.sort((o1, o2) -> Float.compare(o2.getRating(), o1.getRating()));
            return continuacionComunList;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MoveConversionException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public void updateOpening(String fen) {
        JSONObject jsonObject = null;
        try {
            jsonObject = getPositionInfoFromWeb(fen);
            String newOpening = (String) jsonObject.getString("opening_name");
            if (newOpening != null && !newOpening.equals("null")) {
                currentOpening = newOpening;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class GameFragment extends AndroidFragmentApplication {
        private Bundle extras;

        public GameFragment(Bundle extras) {
            this.extras = extras;
        }

        public ChessGameListener listener;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            gdxGame = new ChessGame(extras.getString("san", ""), extras.getBoolean("animate", false));
            gdxGame.listener = listener;
            return initializeForView(gdxGame);
        }
    }

    @Override
    public void exit() {

    }
}
