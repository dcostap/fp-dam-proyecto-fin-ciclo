package com.dcostap.chess;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.dcostap.chess.utils.Utils;

import java.lang.reflect.Field;

public class AndroidUtils {
    public static Drawable drawableForMove(Context context, boolean isWhiteTurn, String move) {
        move = move.toLowerCase();
        move = Utils.normalizeSan(move);
        String png = isWhiteTurn ? "w" : "b";
        if (move.length() == 2)
            png += "p";
        else if (move.charAt(0) == 'n')
            png += "n";
        else if (move.charAt(0) == 'b')
            png += "b";
        else if (move.charAt(0) == 'r')
            png += "r";
        else if (move.charAt(0) == 'k')
            png += "k";
        else if (move.charAt(0) == 'q')
            png += "q";
        else
            return null;

        return context.getDrawable(getResId(png, R.drawable.class));
    }

    public static int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int getRatingPercent(ContinuacionComun continuacionComun) {
        int ratingPercent = (int) (((continuacionComun.getRating() / 2f) + 0.5f) * 100f);
        if (ratingPercent > 100) ratingPercent = 100;
        else if (ratingPercent < 0) ratingPercent = 0;
        return ratingPercent;
    }
}
