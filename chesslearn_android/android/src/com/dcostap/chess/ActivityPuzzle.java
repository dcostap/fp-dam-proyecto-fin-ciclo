package com.dcostap.chess;

import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.core.app.NavUtils;
import androidx.fragment.app.FragmentActivity;

import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.dcostap.chess.utils.Utils;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.move.Move;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;

public class ActivityPuzzle extends FragmentActivity implements AndroidFragmentApplication.Callbacks, ChessGameListener {

    private static final Integer SOUND_MOVE_ID = 212;
    private static final Integer SOUND_CAPTURE_ID = 123;
    private GameFragment fragment;

    static ChessGame gdxGame;
    private LinearLayout linearLayoutBottom;

    private JSONObject puzzleInfo = null;
    private int moveCounter = 0;

    private void updateBottomView(boolean showErrorIcon, String message) {
        runOnUiThread(() -> {
            Toolbar toolbar = findViewById(R.id.toolbar_game2);
            toolbar.setTitle(gdxGame.board.getSideToMove() == Side.BLACK ? getString(R.string.black_to_move) : getString(R.string.white_to_move));
        });
        runOnUiThread(() -> linearLayoutBottom.removeAllViews());

        if (gdxGame == null) return;

        Move lastMove = gdxGame.getLastMove();
        runOnUiThread(() -> {
            View v = LayoutInflater.from(getApplicationContext()).inflate(
                    R.layout.layout_home_tab,
                    linearLayoutBottom,
                    false);

            LinearLayout ll1 = v.findViewById(R.id.home_tab_ll1);
            LinearLayout ll2 = v.findViewById(R.id.home_tab_ll2);
            LinearLayout ll3 = v.findViewById(R.id.home_tab_ll3);

            if (currentOpening != null) {
                TextView tv = new TextView(getApplicationContext());
                tv.setText(currentOpening);
                tv.setTextColor(getResources().getColor(R.color.white));

                ImageView iv = new ImageView(getApplicationContext());
                iv.setImageDrawable(getApplicationContext().getDrawable(R.drawable.book));
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getApplicationContext().getResources().getDisplayMetrics());
                ll1.addView(iv, new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));
                ll1.addView(tv);

                ll1.setOnLongClickListener(v1 -> {
                    Toast.makeText(getApplicationContext(), getText(R.string.opening_name), Toast.LENGTH_SHORT).show();
                    return false;
                });
            }

            System.out.println("move counter: " + moveCounter);
            if (lastMove == null) {
                TextView tv = new TextView(getApplicationContext());
                tv.setText(R.string.try_to_find_right_move);
                tv.setTextSize(18);
                tv.setTextColor(getResources().getColor(R.color.white));
                ll2.addView(tv);
            } else {
                ImageView imageView = null;

                TextView tv = new TextView(getApplicationContext());
                tv.setTextColor(getResources().getColor(R.color.white));
                ImageView ratingImageView = null;
                Drawable ratingDrawable = getApplicationContext().getDrawable(showErrorIcon ? R.drawable.cancel : R.drawable.checkmark);

                ratingImageView = new ImageView(getApplicationContext());
                ratingImageView.setImageDrawable(ratingDrawable);
                tv.setText(message);
                tv.setTextSize(15);

                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getApplicationContext().getResources().getDisplayMetrics());
                if (imageView != null)
                    ll2.addView(imageView, new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));
                ll2.addView(tv);
                tv.setPadding(0, 0, 10, 0);
                if (ratingImageView != null)
                    ll2.addView(ratingImageView, new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            Button button = new Button(getApplicationContext());
            button.setText("Load new puzzle");
            button.setOnClickListener(v1 -> {
                runOnUiThread(() -> linearLayoutBottom.removeAllViews());
                getNewPuzzleFromWeb();
            });
            ll3.addView(button);

            linearLayoutBottom.addView(v);
        });
    }

    private SoundPool soundPool = new SoundPool(2, AudioManager.STREAM_NOTIFICATION, 100);
    private HashMap<Integer, Integer> soundPoolMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        soundPoolMap.put(SOUND_MOVE_ID, soundPool.load(getApplicationContext(), R.raw.move, 1));
        soundPoolMap.put(SOUND_CAPTURE_ID, soundPool.load(getApplicationContext(), R.raw.capture, 1));

        setContentView(R.layout.layout_chess_puzzle);
        linearLayoutBottom = findViewById(R.id.ll_bottom2);

        FrameLayout fl = findViewById(R.id.frame_gdx2);
        fl.getLayoutParams().height = fl.getWidth();

        setActionBar(findViewById(R.id.toolbar_game2));

        getActionBar().setDisplayHomeAsUpEnabled(true);

        fragment = new GameFragment(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_gdx2, fragment)
                .commit();
        fragment.listener = this;
        getNewPuzzleFromWeb();
    }

    String currentOpening = null;

    @Override
    public void onNewBoardPosition(String fen) {

    }

    @Override
    public void onMove(String san) {
        if (puzzleInfo != null) {
            try {
                JSONArray continuations = puzzleInfo.getJSONArray("continuation");
                if (continuations.length() > moveCounter) {
                    String continuation = continuations.getString(moveCounter);
                    if (continuation.equals(san)) {
                        if (continuations.length() > moveCounter + 1) {
                            gdxGame.board.doMove(Utils.decodeSan(gdxGame.board, continuations.getString(moveCounter + 1), gdxGame.board.getSideToMove()));
                            gdxGame.resetGdxPiecesToBoard();
                            moveCounter += 2;
                            updateBottomView(false, "Correct move!");
                        } else {
                            moveCounter++;
                        }

                        if (moveCounter >= continuations.length()) {
                            System.out.println("Finished puzzle");
                            updateBottomView(false, "Finished puzzle!");
                        }
                        soundPool.play(soundPoolMap.get(SOUND_MOVE_ID), 1, 1, 1, 0, 0);
                        return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        updateBottomView(true, "Try again!");
        gdxGame.undoMove();
        gdxGame.resetGdxPiecesToBoard();
    }

    @Override
    public void onCapture(String san) {
        // todo everything is notified as capture?
        soundPool.play(soundPoolMap.get(SOUND_CAPTURE_ID), 1, 1, 1, 0, 0);
        onMove(san);
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    private void getNewPuzzleFromWeb() {
        new Thread(() -> {
            try {
                moveCounter = 0;
                puzzleInfo = readJsonFromUrl("https://dcostap-chess.herokuapp.com/new_puzzle");
                System.out.println(puzzleInfo);
                System.out.println(puzzleInfo.getString("fen"));
                gdxGame.board.loadFromFen(puzzleInfo.getString("fen"));
                gdxGame.resetGdxPiecesToBoard();
                updateBottomView(false, "Find the correct move.");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class GameFragment extends AndroidFragmentApplication {
        private Bundle extras;

        public GameFragment(Bundle extras) {
            this.extras = extras;
        }

        public ChessGameListener listener;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            gdxGame = new ChessGame(extras.getString("san", ""), extras.getBoolean("animate", false));
            gdxGame.listener = listener;
            return initializeForView(gdxGame);
        }
    }

    @Override
    public void exit() {

    }
}
