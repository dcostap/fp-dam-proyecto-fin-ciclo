package com.dcostap.chess;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.FragmentActivity;

import com.badlogic.gdx.backends.android.AndroidFragmentApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ActivityStart extends FragmentActivity implements AndroidFragmentApplication.Callbacks {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_start);
        setActionBar(findViewById(R.id.toolbar1));

        Button btPractice = findViewById(R.id.bt_practice);
        ActivityStart other = this;
        btPractice.setOnClickListener(v -> {
            Intent intent = new Intent(other, ActivityGame.class);
            intent.putExtra("san", "");
            intent.putExtra("animate", false);
            startActivity(intent);
        });

        Button btPuzzle = findViewById(R.id.bt_puzzle);
        btPuzzle.setOnClickListener(v -> {
            Intent intent = new Intent(other, ActivityPuzzle.class);
            intent.putExtra("san", "");
            intent.putExtra("animate", false);
            startActivity(intent);
        });
    }

    @Override
    public void exit() {

    }
}
