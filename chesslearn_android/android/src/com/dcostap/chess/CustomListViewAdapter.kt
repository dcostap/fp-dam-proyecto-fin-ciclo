package com.dcostap.chess

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import java.util.*

class CustomListViewAdapter(private val moves: ArrayList<ContinuacionComun>, private val thisContext: Context, private val isWhiteTurn: Boolean) : ArrayAdapter<Any?>(thisContext, R.layout.adapter_layout) {
    override fun getCount(): Int {
        return moves.size + 1
    }

    override fun getItem(position: Int): Any? {
        return if (position == 0) null else moves[position - 1]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = getItem(position) as ContinuacionComun?
        val v = LayoutInflater.from(thisContext).inflate(R.layout.adapter_layout, parent, false)
        if (item == null) {
            v.findViewById<TextView>(R.id.adapter_tv).setText(R.string.move)
            v.findViewById<ImageView>(R.id.adapter_iv).setImageDrawable(null)

            v.findViewById<LinearLayout>(R.id.ll_adapter_2column).also {
                it.addView(TextView(thisContext).also {
                    it.setText(R.string.rating)
                })
            }

            v.findViewById<LinearLayout>(R.id.ll_adapter_3column).also {
                it.addView(TextView(thisContext).also {
                    it.setText(R.string.popularity)
                })
            }
        } else {
            v.findViewById<TextView>(R.id.adapter_tv).text = item.moveString
            val drawable = AndroidUtils.drawableForMove(thisContext, isWhiteTurn, item.moveString)
            if (drawable != null) {
                v.findViewById<ImageView>(R.id.adapter_iv).setImageDrawable(drawable)
                v.findViewById<ImageView>(R.id.adapter_iv).requestLayout()

                val ratingPercent = AndroidUtils.getRatingPercent(item)

                v.findViewById<LinearLayout>(R.id.ll_adapter_2column).also {

                    it.addView(TextView(thisContext).also {
                        it.text = "$ratingPercent%"
                        it.minWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45f, thisContext.resources.displayMetrics).toInt()

                    }, LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT))

                    val size = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, thisContext.resources.displayMetrics)

                    it.addView(ImageView(thisContext).also {
                        var ratingDrawable = thisContext.getDrawable(R.drawable.cancel)
                        if (ratingPercent >= 60) ratingDrawable = thisContext.getDrawable(R.drawable.star) else if (ratingPercent >= 35) ratingDrawable = thisContext.getDrawable(R.drawable.checkmark)
                        it.setImageDrawable(ratingDrawable)
                    }, LinearLayout.LayoutParams(size.toInt(), size.toInt()))
                }

                v.findViewById<LinearLayout>(R.id.ll_adapter_3column).also {
                    it.addView(TextView(thisContext).also {
                        it.setText(((item.popularity * 100).toInt()).toString() + "%")
                    })
                }
            }
        }
        return v
    }
}