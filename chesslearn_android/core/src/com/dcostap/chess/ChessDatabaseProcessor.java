package com.dcostap.chess;

import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.move.Move;
import com.github.bhlangonijr.chesslib.move.MoveList;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Se usa una base de datos que contiene millones de partidas reales jugadas en Enero en la página lichess.org.
 *
 * https://database.lichess.org/
 *
 * Se analiza cada posición y movimiento en cada juego, y se añade la información a la base de datos que la aplicación
 * usa posteriormente.
 *
 * Para calcular la valoración de cada posición se usa el popular engine de ajedrez de código abierto Stockfish.
 *
 * Usa hilos para acelerar el procesamiento.
 */
public class ChessDatabaseProcessor {
    public static String databasePath = "E:/lichess_db_sliced_2021-01.pgn";
    public static String outputFolder = "./db/";
    public static File dbFileOutput = new File(outputFolder, "db_populated.db");

    /** Cuán profundo (en nº de movimientos realizados) se analiza cada juego */
    private static int gameMoveNumberLimit = 22;

    /** Cuántos hilos usar */
    private static int numberOfThreads = 10;

    private static Semaphore dbAccess = new Semaphore(1);

    public static void main(String[] args) throws IOException, InterruptedException {
        long timeStart = System.currentTimeMillis();
        AtomicInteger gamesProcessed = new AtomicInteger();

        RandomAccessFile f = new RandomAccessFile(new File(databasePath), "r");

        ThreadPoolExecutor pool = (ThreadPoolExecutor) Executors.newFixedThreadPool(numberOfThreads);

        new File(outputFolder).mkdirs();

        // usamos información de en qué byte se estuvo procesando la base de datos de partidas la última vez para poder reanudar en ese punto
        File pointerInfoFile = new File(outputFolder, "pointer_location.txt");
        RandomAccessFile pointerInfoWriter = new RandomAccessFile(pointerInfoFile, "rw");

        long pos = 0;
        if (pointerInfoFile.exists()) {
            try {
                pos = pointerInfoWriter.readLong();
            } catch (IOException e) {

            }
        }

        System.out.println("Reading database file starting from byte position " + pos);
        f.seek(pos);

        boolean readingAGame = false;

        // cierra el archivo en caso de fallo
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    pointerInfoWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        StringBuilder sb = new StringBuilder();

        // leemos la información de la base de datos de partidas y cada partida se añade al pool de Threads para ser procesada
        while (true) {
            boolean firstTime = true;

            // si el pool de Threads está saturado, espera durante un tiempo
            while (pool.getActiveCount() >= pool.getMaximumPoolSize()) {
                if (firstTime) {
                    System.out.println("Waiting for space in thread pool...");
                    long elapsed = System.currentTimeMillis() - timeStart;
                    int i = gamesProcessed.get();

                    System.out.println("Games processed: " + i);
                    if (i > 0)
                        System.out.println("Speed: " + (elapsed / i / 1000f) + " seconds per game");
                }

                firstTime = false;
                Thread.sleep(500);
            }

            String line = f.readLine();

            if (line.startsWith("[Event")) {
                if (readingAGame) {
                    String game = sb.toString();
                    pool.submit(
                            new Thread(() -> {
                                try {
                                    processGame(game);
                                    gamesProcessed.getAndIncrement();
                                } catch (InterruptedException throwables) {
                                    throwables.printStackTrace();
                                }
                            }));
                } else {
                    readingAGame = true;
                }
                sb.setLength(0);
            }

            sb.append(line).append("\n");

            pointerInfoWriter.setLength(0);
            pointerInfoWriter.writeLong(f.getFilePointer());
        }
    }

    enum Result {
        DRAW, WHITE_WINS, BLACK_WINS
    }

    private static void processGame(String gameInfo) throws InterruptedException {
        String[] lines = gameInfo.split("\n");
        Result result = Result.DRAW;
        String moves = null;

        for (String line : lines) {
            if (line.startsWith("[Result")) {
                if (line.equals("[Result \"0-1\"]")) result = Result.BLACK_WINS;
                else if (line.equals("[Result \"1-0\"]")) result = Result.WHITE_WINS;
            } else if (line.startsWith("1.")) {
                moves = line;
                moves = moves.replaceAll("\\{(.*?\\})", "");
                moves = moves.replaceAll("\\d(\\/2)?-\\d(\\/2)?", "");
            } else if (line.startsWith("[Site")) {
                System.out.println("Processing game: " + line);
            }
        }

        if (moves == null) {
            System.out.println("Couldn't retrieve moves...");
            System.out.println();
            for (String line : lines) {
                System.out.println(line);
            }
            return;
        }

        Stockfish stockfish = new Stockfish();
        stockfish.startEngine();

        Board board = new Board();

        MoveList ml = new MoveList();
        ml.loadFromSan(moves);
        int moveNumber = 0;
        int previousContId = -1;
        float previousEval = 0f;

        // simula la partida, jugando cada movimiento que fue realizado
        for (Move move : ml) {
            moveNumber++;

            if (moveNumber >= gameMoveNumberLimit)
                break;

            String fen = board.getFen();
            float evalScore = stockfish.getEvalScore(fen, 10);

            dbAccess.acquire();

            try {
                Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dbFileOutput.getAbsolutePath());
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(15);

                if (previousContId >= 0) {
                    statement.executeUpdate(
                            "UPDATE continuacion_comun SET\n" +
                                    "       valoracion_movimiento = '" + (previousEval - evalScore) + "'\n" +
                                    " WHERE ID_continuacion = '" + previousContId + "';"
                    );
                }

                ResultSet resultSet = statement.executeQuery("select ID_posicion from posicion_tablero where FEN = '" + fen + "';");

                // comprueba si esta posición aún no fue añadida a la base de datos, en cuyo caso se añade
                int idPosicion;
                if (resultSet.isAfterLast()) {
                    if (board.getSideToMove() == Side.BLACK) evalScore *= -1f;

                    statement.executeUpdate(
                            "INSERT INTO posicion_tablero (\n" +
                                    "                                 FEN,\n" +
                                    "                                 valoracion_posicion\n" +
                                    "                             )\n" +
                                    "                             VALUES (\n" +
                                    "                                 '" + fen + "',\n" +
                                    "                                 '" + evalScore + "'\n" +
                                    "                             );"
                    );

                    resultSet = statement.executeQuery("select ID_posicion from posicion_tablero where FEN = '" + fen + "';");
                }

                idPosicion = resultSet.getInt(1);

                statement.setQueryTimeout(15);

                ResultSet resultSet2 = statement.executeQuery("select ID_continuacion, veces_jugado, victorias_blanco, " +
                        "empates, victorias_negro from continuacion_comun where ID_posicion = '" + idPosicion + "' and " +
                        "movimiento = '" + move.getSan() + "';");

                if (resultSet2.isAfterLast()) {
                    statement.executeUpdate(
                            "INSERT INTO continuacion_comun (\n" +
                                    "                                   ID_posicion,\n" +
                                    "                                   movimiento,\n" +
                                    "                                   veces_jugado,\n" +
                                    "                                   victorias_blanco,\n" +
                                    "                                   victorias_negro,\n" +
                                    "                                   empates,\n" +
                                    "                                   valoracion_movimiento\n" +
                                    "                               )\n" +
                                    "                               VALUES (\n" +
                                    "                                   " + idPosicion + ",\n" +
                                    "                                   '" + move.getSan() + "',\n" +
                                    "                                   '" + 1 + "',\n" +
                                    "                                   '" + (result == Result.WHITE_WINS ? 1 : 0) + "',\n" +
                                    "                                   '" + (result == Result.BLACK_WINS ? 1 : 0) + "',\n" +
                                    "                                   '" + (result == Result.DRAW ? 1 : 0) + "',\n" +
                                    "                                   '" + null + "'\n" +
                                    "                               );"
                    );
                } else {
                    int idCont = resultSet2.getInt(1);
                    int timesPlayed = resultSet2.getInt(2);
                    int whiteWins = resultSet2.getInt(3);
                    int draws = resultSet2.getInt(4);
                    int blackWins = resultSet2.getInt(5);

                    timesPlayed++;
                    switch (result) {
                        case DRAW:
                            draws++;
                            break;
                        case WHITE_WINS:
                            whiteWins++;
                            break;
                        case BLACK_WINS:
                            blackWins++;
                            break;
                    }

                    statement.executeUpdate(
                            "UPDATE continuacion_comun SET\n" +
                                    "       veces_jugado = '" + timesPlayed + "',\n" +
                                    "       victorias_blanco = '" + whiteWins + "',\n" +
                                    "       empates = '" + draws + "',\n" +
                                    "       victorias_negro = '" + blackWins + "'\n" +
                                    " WHERE ID_continuacion = '" + idCont + "';"
                    );
                }

                ResultSet resultSet3 = statement.executeQuery("select ID_continuacion from " +
                        "continuacion_comun where ID_posicion = '" + idPosicion + "' and " +
                        "movimiento = '" + move.getSan() + "';");
                previousContId = resultSet3.getInt(1);
                previousEval = evalScore;
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            dbAccess.release();

            board.doMove(move);
        }

        stockfish.stopEngine();
    }
}
