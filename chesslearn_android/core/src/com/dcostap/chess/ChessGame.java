package com.dcostap.chess;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.dcostap.chess.utils.Utils;
import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import com.github.bhlangonijr.chesslib.move.MoveList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.concurrent.Semaphore;

/** Clase que se encarga de dibujar el tablero de ajedrez usando libgdx y OpenGL. El resultado se incrusta
 * en un View de Android */
public class ChessGame extends ApplicationAdapter implements InputProcessor {
    SpriteBatch batch;
    ShapeRenderer shapeRenderer;
    Board board;

    private MoveList openingAnimation;
    private float openingAnimationTimer = 0f;

    public ChessGameListener listener;

    // todas las imágenes y colores que se usan
    HashMap<Piece, Texture> textures = new HashMap<>();
    Texture pixel;
    Texture circle;
    Texture marker;
    Color darkSquaresColor;
    Color lightSquaresColor;
    Color lightSquaresColorHighlight;
    Color darkSquaresColorHighlight;
    Color highlightPositionCircleColor;
    Color highlightMovementCircleColor;

    /** La lista de las piezas gráficas.
     * @see ChessVisualPiece */
    List<ChessVisualPiece> chessVisualPieces = Collections.synchronizedList(new ArrayList<>());

    ChessVisualPiece selectedPiece = null;

    int boardSquareSize;

    /** El movimiento para el cual se dibuja una flecha en el tablero que lo muestra de forma visual */
    Move highlightMove = null;

    private Semaphore lock = new Semaphore(1);

    public ChessGame(String san, boolean startWithAnimation) {
        this.board = new Board();

        MoveList ml = new MoveList();
        ml.loadFromSan(san);

        if (startWithAnimation) {
            openingAnimation = ml;
        } else {
            for (Move m : ml) {
                doMove(m);
            }
        }
    }

    private Stack<Move> undoneMoves = new Stack<>();

    public void undoMove() {
        if (!allowExternalChanges()) return;

        try {
            lock.acquire();
        } catch (InterruptedException e) {
            return;
        }

        highlightMove = null;

        Move move = null;
        try {
            move = board.undoMove();
        } catch (Exception e) {

        }
        if (move != null) {
            undoneMoves.push(move);
            animateGdxPiecesToBoard();
        }

        lock.release();
    }

    /** Si se está animando no se permite que haya más movimientos, para evitar problemas de sincronización entre las piezas dibujadas
     * y las piezas reales */
    private boolean allowExternalChanges() {
        return isAnimatingTimer < 0f && openingAnimation == null;
    }

    public void redoMove() {
        if (!allowExternalChanges()) return;

        try {
            lock.acquire();
        } catch (InterruptedException e) {
            return;
        }
        highlightMove = null;

        try {
            if (undoneMoves.size() > 0) {
                board.doMove(undoneMoves.pop());
                animateGdxPiecesToBoard();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        lock.release();
    }

    public void doMove(Move move) {
        if (!allowExternalChanges()) return;
        highlightMove = null;
        undoneMoves.clear();

        String san = Utils.encodeToSan(board, move);

        if (board.getPiece(move.getFrom()).getPieceType() != null)
            board.doMove(move);

        if (board.getPiece(move.getTo()) != Piece.NONE)
            listener.onCapture(san);
        else
            listener.onMove(san);
    }

    @Override
    public void create() {
        boardSquareSize = Gdx.graphics.getWidth() / 8;

        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();

        // se vincula cada tipo de pieza con su imagen
        textures.put(Piece.BLACK_ROOK, new Texture("bR.png"));
        textures.put(Piece.BLACK_PAWN, new Texture("bP.png"));
        textures.put(Piece.BLACK_KNIGHT, new Texture("bN.png"));
        textures.put(Piece.BLACK_BISHOP, new Texture("bB.png"));
        textures.put(Piece.BLACK_KING, new Texture("bK.png"));
        textures.put(Piece.BLACK_QUEEN, new Texture("bQ.png"));

        textures.put(Piece.WHITE_ROOK, new Texture("wR.png"));
        textures.put(Piece.WHITE_PAWN, new Texture("wP.png"));
        textures.put(Piece.WHITE_KNIGHT, new Texture("wN.png"));
        textures.put(Piece.WHITE_BISHOP, new Texture("wB.png"));
        textures.put(Piece.WHITE_KING, new Texture("wK.png"));
        textures.put(Piece.WHITE_QUEEN, new Texture("wQ.png"));

        pixel = new Texture("pixel.png");
        circle = new Texture("circle.png");
        marker = new Texture("marker.png");

        darkSquaresColor = new Color(140 / 255f, 162 / 255f, 173 / 255f, 1f);
        lightSquaresColor = new Color(222 / 255f, 227 / 255f, 230 / 255f, 1f);

        darkSquaresColorHighlight = new Color(140 / 255f, 192 / 255f, 173 / 255f, 1f);
        lightSquaresColorHighlight = new Color(222 / 255f, 267 / 255f, 220 / 255f, 1f);

        highlightPositionCircleColor = new Color(Color.DARK_GRAY);
        highlightPositionCircleColor.a = 0.26f;

        highlightMovementCircleColor = new Color(Color.BLACK);
        highlightMovementCircleColor.a = 0.33f;
        Gdx.input.setInputProcessor(this);

        resetGdxPiecesToBoard();
    }

    /** Fuerza una sincronización entre las piezas reales y las piezas dibujadas, resetando cualquier animación en proceso */
    public void resetGdxPiecesToBoard() {
        highlightMove = null;
        isAnimatingTimer = -1f;
        chessVisualPieces.clear();
        for (int i = 0; i < 64; i++) {
            Square square = Square.squareAt(i);
            Piece piece = board.getPiece(square);

            if (piece == Piece.NONE) continue;

            int xPos = (i % 8) * boardSquareSize;
            int yPos = (i / 8) * boardSquareSize;
            ChessVisualPiece chessVisualPiece = new ChessVisualPiece(piece, xPos + boardSquareSize * 0.01f, yPos + boardSquareSize * 0.01f, this);
            chessVisualPieces.add(chessVisualPiece);
        }
        if (listener != null) {
            listener.onNewBoardPosition(board.getFen());
        }
    }

    /**
     * < 0 = finished animating.
     * While animating some actions are disabled. When just finished animation, board is reset.
     */
    private float isAnimatingTimer = -1f;

    /**
     * Encuentra los cambios entre la posición actual y la nueva y aplica las animaciones relevantes.
     *
     * Utiliza un método de comparación para adivinar el tipo de movimiento realizado, de esta forma no
     * necesita más información que el tablero anterior y el actual.
     */
    public void animateGdxPiecesToBoard() {
        HashMap<ChessVisualPiece, Square> previousSquares = new HashMap<>();
        ArrayList<ChessVisualPiece> toBeRemoved = new ArrayList<>();

        try {
            for (ChessVisualPiece chessVisualPiece : chessVisualPieces) {
                Square square = squareForPosition(chessVisualPiece.pos.x, chessVisualPiece.pos.y);
                previousSquares.put(chessVisualPiece, square);
            }
            isAnimatingTimer = 0.16f; // todo this timer decreases with delta but piece animations aren't based on delta

            for (ChessVisualPiece chessVisualPiece : chessVisualPieces) {
                Square previousSquare = previousSquares.get(chessVisualPiece);
                Piece pieceInOldSquare = board.getPiece(previousSquare);
                Piece piece = chessVisualPiece.piece;

                // la pieza cambió de posición o desapareció, detecta si esto constituye un movimiento, en cuyo caso ya sabemos qué animar
                if (pieceInOldSquare == Piece.NONE || pieceInOldSquare.getPieceSide() != chessVisualPiece.piece.getPieceSide()) {
                    Square squareWherePieceIsAtNow = null;
                    for (Square square : board.getPieceLocation(chessVisualPiece.piece)) {
                        if (square == Square.NONE) continue;

                        boolean wasSquareOccupiedBySameSidePiece = false;
                        for (Map.Entry<ChessVisualPiece, Square> entry : previousSquares.entrySet()) {
                            if (entry.getKey().piece.getPieceSide() == piece.getPieceSide() && entry.getKey().piece.getPieceType() == piece.getPieceType())
                                if (entry.getValue().equals(square)) {
                                    wasSquareOccupiedBySameSidePiece = true;
                                    break;
                                }
                        }

                        if (!wasSquareOccupiedBySameSidePiece) {
                            squareWherePieceIsAtNow = square;
                            break;
                        }
                    }

                    if (squareWherePieceIsAtNow != null) {
                        // piece moved
                        chessVisualPiece.animatePosition = true;
                        chessVisualPiece.desiredPos = positionForSquare(squareWherePieceIsAtNow);
                    } else {
                        // piece disappeared
                        toBeRemoved.add(chessVisualPiece);

                        int currentPieceCount = 0;
                        for (Square sq : Square.values()) {
                            if (sq != Square.NONE) {
                                if (board.getPiece(sq) != Piece.NONE) {
                                    currentPieceCount++;
                                }
                            }
                        }

                        // Ha desaparecido una pieza, pero el contador de piezas es igual? Significa que un peón ha sido coronado
                        if (currentPieceCount != chessVisualPieces.size() - 1) {
                            // Las animaciones para promociones de peones no funcionan,
                            // por lo que parchée un workaround en touchDown para deshabilitar animaciones en estos casos
                            resetGdxPiecesToBoard();
                            return;
                        }
                    }
                }
            }
        } catch (Exception e) {
            resetGdxPiecesToBoard();
            return;
        }

        for (ChessVisualPiece chessVisualPiece : toBeRemoved) {
            chessVisualPieces.remove(chessVisualPiece);
        }
    }

    public int getMovesMade() {
        return (board.getMoveCounter() * 2) - (board.getSideToMove() == Side.WHITE ? 1 : 0);
    }

    Vector2 positionForSquare(Square square) {
        int i = square.ordinal();
        int xPos = (i % 8) * boardSquareSize;
        int yPos = (i / 8) * boardSquareSize;
        return new Vector2(xPos, yPos);
    }

    Square squareForPosition(float x, float y) {
        int xIndex = (int) Math.max(Math.min(x / boardSquareSize, 7), 0);
        int yIndex = (int) Math.max(Math.min(y / boardSquareSize, 7), 0);
        return Square.squareAt(xIndex + yIndex * 8);
    }

    public Move getLastMove() {
        if (board.getBackup().size() > 0)
            return board.getBackup().getLast().getMove();
        return null;
    }

    /** Método que dibuja lo necesario*/
    @Override
    public void render() {
        Gdx.gl.glClearColor(0.95f, 0.95f, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (isAnimatingTimer > 0f) {
            isAnimatingTimer -= Gdx.graphics.getDeltaTime();
            if (isAnimatingTimer <= 0f)
                resetGdxPiecesToBoard();
        }

        // sistema para animar una apertura al inicio de la partida, no implementado en la app final
        if (openingAnimation != null) {
            openingAnimationTimer += Gdx.graphics.getDeltaTime();
            if (openingAnimationTimer > 0.5f) {
                openingAnimationTimer = 0f;

                doMove(openingAnimation.pop());
                animateGdxPiecesToBoard();

                if (openingAnimation.isEmpty()) {
                    openingAnimation = null;
                }
            }
        }

        // comenzamos el dibujado de gráficos OpenGL
        batch.begin();

        boolean isDarkSquare = true;

        Move lastMove = getLastMove();

        // dibujamos las casillas del tablero, alternando colores
        for (int i = 0; i < 64; i++) {
            Square square = Square.squareAt(i);
            Piece piece = board.getPiece(square);
            Vector2 pos = positionForSquare(square);

            batch.setColor(isDarkSquare ? darkSquaresColor : lightSquaresColor);

            if (lastMove != null)
                if (lastMove.getFrom().equals(square) || lastMove.getTo().equals(square))
                    batch.setColor(isDarkSquare ? darkSquaresColorHighlight : lightSquaresColorHighlight);

            batch.draw(pixel, pos.x, pos.y, boardSquareSize, boardSquareSize);
            batch.setColor(Color.WHITE);

            isDarkSquare = !isDarkSquare;
            if (i % 8 == 7) isDarkSquare = !isDarkSquare;
        }

        // dibujamos elementos de la interfaz para cuando una pieza está seleccionada
        // estos elementos facilitan la interacción marcando con claridad la casilla que está pro debajo de la pieza
        // que se está arrastrando; casilla la cual el dedo del usuario probablemente tape
        if (selectedPiece != null) {
            if (selectedPiece.beingDragged) {
                batch.setColor(highlightPositionCircleColor);
                Vector2 pos = positionForSquare(squareForPosition(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY()));
                float size = boardSquareSize * 1.8f;


                batch.draw(circle, pos.x - size / 2f + boardSquareSize / 2f, pos.y - size / 2f + boardSquareSize / 2f, 0f, 0f,
                        size, size, 1f, 1f,
                        0f, 0, 0, circle.getWidth(), circle.getHeight(), false, false);
                batch.setColor(Color.WHITE);
            }
        }

        try {
            // dibuja todas las piezas, ordenado por la prioridad de dibujado de cada pieza
            chessVisualPieces.sort(new Comparator<ChessVisualPiece>() {
                @Override
                public int compare(ChessVisualPiece chessVisualPiece, ChessVisualPiece t1) {
                    return Integer.compare(chessVisualPiece.drawingPriority, t1.drawingPriority);
                }
            });

            for (ChessVisualPiece chessVisualPiece : chessVisualPieces) {
                chessVisualPiece.draw();
            }
        } catch (Exception e) {

        }

        // dibuja ayudas que marcan las posibles casillas a las que puede ir la pieza selecciona
        if (selectedPiece != null) {
            for (Move move : board.legalMoves()) {
                if (move.getFrom().equals(squareForPosition(selectedPiece.touchPieceOrigin.x, selectedPiece.touchPieceOrigin.y))) {
                    batch.setColor(highlightMovementCircleColor);
                    Vector2 pos = positionForSquare(move.getTo());

                    if (board.getPiece(move.getTo()) != Piece.NONE) {
                        float size = boardSquareSize * 1f;
                        batch.draw(marker, pos.x + (boardSquareSize - size) / 2f, pos.y + (boardSquareSize - size) / 2f, 0f, 0f,
                                size, size, 1f, 1f,
                                0f, 0, 0, marker.getWidth(), marker.getHeight(), false, false);
                    } else {
                        float size = boardSquareSize * 0.3f;
                        batch.draw(circle, pos.x - size / 2f + boardSquareSize / 2f, pos.y - size / 2f + boardSquareSize / 2f, 0f, 0f,
                                size, size, 1f, 1f,
                                0f, 0, 0, circle.getWidth(), circle.getHeight(), false, false);
                    }
                    batch.setColor(Color.WHITE);
                }
            }
        }


        // Se dibuja una flecha en el tablero que muestra un movimiento de forma visual
        if (highlightMove != null) {
            float thickness = boardSquareSize / 10f;
            batch.setColor(0f / 255f, 107f / 255f, 80f / 255f, 1f);
            Vector2 origin = positionForSquare(highlightMove.getFrom());
            Vector2 destination = positionForSquare(highlightMove.getTo());
            float half = boardSquareSize / 2f;
            drawLine(origin.x + half, origin.y + half, destination.x + half, destination.y + half, thickness, true, boardSquareSize / 1.5f);
        }

        batch.end();
    }

    /** Dibuja una flecha usando cálculos matemáticos para dibujar las 3 líneas de las que se compone */
    private void drawLine(float x1, float y1, float x2, float y2, float thickness, boolean isArrow,
                          float arrowSize) {
        float dx = x2 - x1;
        float dy = y2 - y1;
        float dist = (float) Math.sqrt((dx * dx + dy * dy));
        float deg = (float) Math.toDegrees(Math.atan2(dy, dx));

        // base de la flecha
        batch.draw(pixel, x1, y1, 0f, thickness / 2f, dist, thickness, 1f, 1f, deg, 0, 0, pixel.getWidth(), pixel.getHeight(), false, false);

        float angle = (float) Math.toDegrees(Math.atan2((y2 - y1), (x2 - x1)));
        if (angle < 0) angle += 360f;

        if (isArrow) {
            float angleRad = (float) Math.toRadians(angle);
            float angleDiff = -40;
            float angle1 = angleRad + angleDiff;
            float angle2 = angleRad - angleDiff;
            // la cabeza de la flecha
            drawLine(x2, y2, (float) (x2 + (Math.cos(angle1) * arrowSize)), (float) (y2 + (Math.sin(angle1) * arrowSize)), thickness, false, arrowSize);
            drawLine(x2, y2, (float) (x2 + (Math.cos(angle2) * arrowSize)), (float) (y2 + (Math.sin(angle2) * arrowSize)), thickness, false, arrowSize);
        }
    }

    /** El eje de coordenadas Y está invertido en OpenGL por lo que aquí se realiza la conversión */
    private int screenYToGameY(int screenY) {
        return Gdx.graphics.getHeight() - screenY;
    }

    @Override
    public void dispose() {
        batch.dispose();
        for (Texture t : textures.values()) t.dispose();
        pixel.dispose();
        circle.dispose();
        marker.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.keyDown(keycode)) return true;
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.keyUp(keycode)) return true;
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.keyTyped(character)) return true;
        return false;
    }

    /** Evento cuando se toca en el tablero con el dedo. Procesamos el arrastre de piezas, teniendo en cuenta que hay 2 tipos
     * diferentes de arrastres. */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!allowExternalChanges()) return false;
        screenY = screenYToGameY(screenY);
        if (selectedPiece != null) {
            Square touchSquare = squareForPosition(screenX, screenY);
            Square originSquare = squareForPosition(selectedPiece.pos.x, selectedPiece.pos.y);
            Optional<Move> move = board.legalMoves()
                    .stream()
                    .filter(m -> m.getTo().equals(touchSquare) && m.getFrom().equals(originSquare))
                    .findFirst();

            if (move.isPresent()) {
                Move theMove = move.get();
                doMove(theMove);

                System.out.println(move.get().getPromotion());
                // workaround for unsupported promoting animations: just don't animate
                if (move.get().getPromotion() != Piece.NONE)
                    resetGdxPiecesToBoard();
                else
                    animateGdxPiecesToBoard();
                selectedPiece = null;
                return true;
            }
        }

        selectedPiece = null;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.touchDown(screenX, screenY, pointer, button)) return true;
        return false;
    }

    /** Evento cuando se levanta el dedo del tablero con el dedo */
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.touchUp(screenX, screenYToGameY(screenY), pointer, button)) return true;
        return false;
    }

    /** Evento cuando se arrastra el dedo sobre el tablero*/
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.touchDragged(screenX, screenYToGameY(screenY), pointer)) return true;
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.mouseMoved(screenX, screenYToGameY(screenY))) return true;
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (!allowExternalChanges()) return false;
        for (ChessVisualPiece chessVisualPiece : chessVisualPieces)
            if (chessVisualPiece.scrolled(amountX, amountY)) return true;
        return false;
    }
}
