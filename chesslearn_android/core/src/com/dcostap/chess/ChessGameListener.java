package com.dcostap.chess;

/** Para las clases que quieran escuchar eventos del tablero de ajedrez */
public interface ChessGameListener {
    /** La posición ha cambiado */
    void onNewBoardPosition(String fen);
    /** Ha habido un movimiento (que no ha sido captura)
     * @param san*/
    void onMove(String san);
    /** Ha habido una captura
     * @param san*/
    void onCapture(String san);
}
