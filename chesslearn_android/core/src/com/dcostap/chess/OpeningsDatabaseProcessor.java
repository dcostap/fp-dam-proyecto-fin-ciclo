package com.dcostap.chess;

import com.dcostap.chess.utils.Utils;
import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.move.Move;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

/** Esta clase procesa un link a un json que contiene todos los registros oficiales de la Enciclopedia de aperturas de ajedrez (ECO).
 * Añade la información a la base de datos cuando la posición de una apertura se encuentre dentro. */
public class OpeningsDatabaseProcessor {
    private static String URL = "https://raw.githubusercontent.com/hayatbiralem/eco.json/master/eco.json";

    public static void main(String[] args) throws IOException, ParseException, SQLException {
        JSONParser parser = new JSONParser();
        URLConnection conn = new URL(URL).openConnection();

        String jsonString;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
            jsonString = reader.lines().collect(Collectors.joining("\n"));
        }

        Connection connection = DriverManager.getConnection("jdbc:sqlite:" + ChessDatabaseProcessor.dbFileOutput.getAbsolutePath());

        JSONArray json = (JSONArray) parser.parse(jsonString);
        Statement statement = connection.createStatement();

        // iteramos sobre cada apertura definida en el json
        for (Object entry : json) {
            JSONObject entryJson = (JSONObject) entry;

            // simulamos los movimientos que definen la apertura en el tablero de ajedrez, para así generar el FEN y poder buscarlo
            // en la base de datos
            Board board = new Board();

            for (String bothMoves : ((String) entryJson.get("moves")).split("\\d\\.")) {
                for (String san : bothMoves.split(" ")) {
                    san = san.trim();
                    if (san.length() <= 1) continue;
                    Move move = Utils.decodeSan(board, san, board.getSideToMove());
                    board.doMove(move);
                }
            }

            String query = "UPDATE posicion_tablero SET\n" +
                    "       nombre_apertura = \"" + entryJson.get("name") + "\"\n" +
                    " WHERE FEN = '" + board.getFen() + "';";

            statement.executeUpdate(
                    query
            );
        }

        connection.close();
    }
}
