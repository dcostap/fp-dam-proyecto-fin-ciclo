package com.dcostap.chess;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;

import java.util.Optional;

/**
 * Una representación visual de las piezas del tablero de ajedrez.
 *
 * Hay que tener en cuenta que no son las mismas que las piezas reales
 * (las cuales son manejadas por la librería que procesa las reglas del juego, chesslib).
 *
 * Éstas piezas son meramente una representación gráfica, y posibilitan poder animar sus movimientos,
 * por lo cual no siempre van a coincidir con las piezas reales.
 *
 * {@link ChessGame} se encarga de sincronizar las piezas reales con cada pieza visual.
 */
public class ChessVisualPiece implements InputProcessor {
    /** posición actual */
    Vector2 pos = new Vector2();
    /** posición deseada, si no coincide con la posición actual se anima el movimiento hasta que coincidan */
    Vector2 desiredPos = new Vector2();

    private Vector2 scale = new Vector2(1f, 1f);
    private Vector2 desiredScale = new Vector2(scale);

    Piece piece;
    ChessGame game;

    /** El rectángulo que representa la capa de colisión que permite que un input del usuario afecte a esta pieza */
    Rectangle boundingBox = new Rectangle();

    /** La prioridad de dibujado, que permite que algunas piezas se dibujen por encima de otras de forma controlada. Esto
     * permite que al arrastrar una pieza, ésta no sea opacada por el resto */
    int drawingPriority = 0;

    boolean beingDragged = false;
    boolean wasTouched = false;
    boolean animatePosition = false;

    // vectores usados para cálculos con respecto al arrastre de piezas
    Vector2 touchOrigin = new Vector2();
    Vector2 touchPieceOrigin = new Vector2();

    public ChessVisualPiece(Piece piece, float x, float y, ChessGame game) {
        this.piece = piece;
        this.game = game;
        moveTo(x, y);
    }

    /** Mover la pieza a una posición de forma instantánea, sin animación */
    public void moveTo(float x, float y) {
        this.pos.set(x, y);
        this.desiredPos.set(pos);
    }

    public void draw() {
        if (game.selectedPiece == this)
            drawingPriority = 10;
        else
            drawingPriority = 0;

        // animaciones de cambios de escala
        if (beingDragged)
            desiredScale.set(1.7f, 1.7f);
        else if (game.selectedPiece == this)
            desiredScale.set(1.1f, 1.1f);
        else
            desiredScale.set(1f, 1f);

        // animaciones de cambios de posición
        if (!pos.equals(desiredPos)) {
            if (animatePosition) {
                // se calcula la diferencia entre la posición deseada y la actual, y se transiciona mediante
                // una interpolación lineal simple, donde se avanza un 40% cada frame
                Vector2 diff = new Vector2(pos);
                diff.sub(desiredPos);
                if (diff.len() < game.boardSquareSize * 0.07f) {
                    animatePosition = false;
                }
                pos.x = MathUtils.lerp(pos.x, desiredPos.x, 0.4f);
                pos.y = MathUtils.lerp(pos.y, desiredPos.y, 0.4f);
            } else {
                pos.set(desiredPos);
            }
        }

        // animar la escala usando la misma interpolación lineal que para la posición
        if (!scale.equals(desiredScale)) {
            scale.x = MathUtils.lerp(scale.x, desiredScale.x, 0.5f);
            scale.y = MathUtils.lerp(scale.y, desiredScale.y, 0.5f);
        }

        boundingBox.set(pos.x, pos.y, game.boardSquareSize, game.boardSquareSize);

        // cálculos de tamaño para adaptar la textura que se va a dibujar al tamaño adecuado
        float aspectRatio = game.textures.get(piece).getWidth() / (float) game.textures.get(piece).getHeight();
        float width = game.boardSquareSize;
        float height = (game.boardSquareSize) * aspectRatio;

        if (game.selectedPiece == this)
            game.batch.setColor(0.92f, 0.96f, 1f, 1f);
        else
            game.batch.setColor(1f, 1f, 1f, 1f);

        game.batch.draw(game.textures.get(piece), pos.x, pos.y, width / 2f, height / 2f,
                width, height, scale.x, scale.y,
                0f, 0, 0, game.textures.get(piece).getWidth(), game.textures.get(piece).getHeight(), false, false);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    /** Evento cuando se toca en el tablero con el dedo */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        wasTouched = false;
        beingDragged = false;

        // detectar si ésta pieza está debajo de la posición donde se realizó el input
        if (game.board.getSideToMove() == piece.getPieceSide() && boundingBox.contains(screenX, screenY)) {
            touchOrigin.set(screenX, screenY);
            touchPieceOrigin.set(pos);
            wasTouched = true;
            return true;
        }

        return false;
    }

    /** Evento cuando se levanta el dedo del tablero con el dedo */
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (wasTouched) {
            if (beingDragged) {
                // se estaba arrastrando una pieza, por lo cual ahora calculamos si la casilla hacia donde
                // terminó siendo arrastrada representa un movimiento válido, en cuyo caso se realiza dicho movimiento
                game.selectedPiece = null;
                pos.set(touchPieceOrigin);
                desiredPos.set(touchPieceOrigin);

                Square hoverSquare = game.squareForPosition(screenX, screenY);
                Square originSquare = game.squareForPosition(pos.x, pos.y);
                Optional<Move> move = game.board.legalMoves()
                        .stream()
                        .filter(m -> m.getTo().equals(hoverSquare) && m.getFrom().equals(originSquare))
                        .findFirst();

                if (move.isPresent()) {
                    game.doMove(move.get());
                    game.resetGdxPiecesToBoard();
                }
            } else {
                game.selectedPiece = this;
            }

            beingDragged = false;
            wasTouched = false;
            return true;
        }
        return false;
    }

    /** Evento cuando se arrastra el dedo sobre el tablero*/
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (beingDragged) {
            desiredPos.x = touchPieceOrigin.x + (screenX - touchOrigin.x);
            desiredPos.y = touchPieceOrigin.y + (screenY - touchOrigin.y);
            return true;
        } else if (wasTouched) {
            // margen de distancia para evitar que se arrastre una pieza sin querer
            if (Math.max(Math.abs(screenX - touchOrigin.x), Math.abs(screenY - touchOrigin.y)) > game.boardSquareSize * 0.29f) {
                beingDragged = true;
                animatePosition = true;
                game.selectedPiece = this;
            }
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
