package com.dcostap.chess.utils;

import com.github.bhlangonijr.chesslib.Bitboard;
import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.Constants;
import com.github.bhlangonijr.chesslib.File;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.PieceType;
import com.github.bhlangonijr.chesslib.Rank;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import com.github.bhlangonijr.chesslib.move.MoveConversionException;
import com.github.bhlangonijr.chesslib.util.StringUtil;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {
    private static EnumMap<Piece, String> sanNotation =
            new EnumMap<Piece, String>(Piece.class);

    private static EnumMap<Piece, String> fanNotation =
            new EnumMap<Piece, String>(Piece.class);

    private static Map<String, PieceType> sanNotationR =
            new HashMap<String, PieceType>(7);

    static {
        sanNotation.put(Piece.WHITE_PAWN, "");
        sanNotation.put(Piece.BLACK_PAWN, "");
        sanNotation.put(Piece.WHITE_KNIGHT, "N");
        sanNotation.put(Piece.BLACK_KNIGHT, "N");
        sanNotation.put(Piece.WHITE_BISHOP, "B");
        sanNotation.put(Piece.BLACK_BISHOP, "B");
        sanNotation.put(Piece.WHITE_ROOK, "R");
        sanNotation.put(Piece.BLACK_ROOK, "R");
        sanNotation.put(Piece.WHITE_QUEEN, "Q");
        sanNotation.put(Piece.BLACK_QUEEN, "Q");
        sanNotation.put(Piece.WHITE_KING, "K");
        sanNotation.put(Piece.BLACK_KING, "K");
        sanNotation.put(Piece.NONE, "NONE");

        fanNotation.put(Piece.WHITE_PAWN, "♙");
        fanNotation.put(Piece.BLACK_PAWN, "♟");
        fanNotation.put(Piece.WHITE_KNIGHT, "♘");
        fanNotation.put(Piece.BLACK_KNIGHT, "♞");
        fanNotation.put(Piece.WHITE_BISHOP, "♗");
        fanNotation.put(Piece.BLACK_BISHOP, "♝");
        fanNotation.put(Piece.WHITE_ROOK, "♖");
        fanNotation.put(Piece.BLACK_ROOK, "♜");
        fanNotation.put(Piece.WHITE_QUEEN, "♕");
        fanNotation.put(Piece.BLACK_QUEEN, "♛");
        fanNotation.put(Piece.WHITE_KING, "♔");
        fanNotation.put(Piece.BLACK_KING, "♚");
        fanNotation.put(Piece.NONE, "NONE");

        sanNotationR.put("", PieceType.PAWN);
        sanNotationR.put("N", PieceType.KNIGHT);
        sanNotationR.put("B", PieceType.BISHOP);
        sanNotationR.put("R", PieceType.ROOK);
        sanNotationR.put("Q", PieceType.QUEEN);
        sanNotationR.put("K", PieceType.KING);
        sanNotationR.put("NONE", PieceType.NONE);
    }

    /** Este método devuelve el Move correspondiente a un string SAN, que identifica un movimiento. SAN puede tener diversos
     * formatos por lo que este método decodifica cuaqluier variante */
    public static Move decodeSan(Board board, String san, Side side) throws MoveConversionException {
        if (san.equalsIgnoreCase("Z0")) {
            return null;
        }
        san = normalizeSan(san);

        String strPromotion = StringUtil.afterSequence(san, "=", 1);
        san = StringUtil.beforeSequence(san, "=");

        char lastChar = san.charAt(san.length() - 1);
        //FIX missing equal sign for pawn promotions
        if (Character.isLetter(lastChar) && Character.toUpperCase(lastChar) != 'O') {
            san = san.substring(0, san.length() - 1);
            strPromotion = lastChar + "";
        }

        if (san.equals("O-O") || san.equals("O-O-O")) { // is castle
            if (san.equals("O-O")) {
                return board.getContext().getoo(side);
            } else {
                return board.getContext().getooo(side);
            }
        }

        if (san.length() == 3 &&
                Character.isUpperCase(san.charAt(2))) {
            strPromotion = san.substring(2, 3);
            san = san.substring(0, 2);
        }

        Square from = Square.NONE;
        Square to;
        try {
            to = Square.valueOf(StringUtil.lastSequence(san.toUpperCase(), 2));
        } catch (Exception e) {
            throw new MoveConversionException("Coudn't parse destination square[" + san + "]: " +
                    san.toUpperCase());
        }
        Piece promotion = strPromotion.equals("") ? Piece.NONE :
                Constants.getPieceByNotation(side.equals(Side.WHITE) ?
                        strPromotion.toUpperCase() : strPromotion.toLowerCase());

        if (san.length() == 2) { //is pawn move
            long mask = Bitboard.getBbtable(to) - 1L;
            long xfrom = (side.equals(Side.WHITE) ? mask : ~mask) & Bitboard.getFilebb(to) &
                    board.getBitboard(Piece.make(side, PieceType.PAWN));
            int f = side.equals(Side.BLACK) ? Bitboard.bitScanForward(xfrom) :
                    Bitboard.bitScanReverse(xfrom);
            if (f >= 0 && f <= 63) {
                from = Square.squareAt(f);
            }
        } else {

            String strFrom = (san.contains("x") ?
                    StringUtil.beforeSequence(san, "x") :
                    san.substring(0, san.length() - 2));

            if (strFrom == null ||
                    strFrom.length() == 0 || strFrom.length() > 3) {
                throw new MoveConversionException("Couldn't parse 'from' square " + san + ": Too many/few characters.");
            }

            PieceType fromPiece = PieceType.PAWN;

            if (Character.isUpperCase(strFrom.charAt(0))) {
                fromPiece = sanNotationR.get(strFrom.charAt(0) + "");
            }

            if (strFrom.length() == 3) {
                from = Square.valueOf(strFrom.substring(1, 3).toUpperCase());
            } else {
                String location = "";
                if (strFrom.length() == 2) {
                    if (Character.isUpperCase(strFrom.charAt(0))) {
                        location = strFrom.substring(1, 2);
                    } else {
                        location = strFrom.substring(0, 2);
                        from = Square.valueOf(location.toUpperCase());
                    }
                } else {
                    if (Character.isLowerCase(strFrom.charAt(0))) {
                        location = strFrom;
                    }
                }
                if (location.length() < 2) {
                    //resolving ambiguous from
                    long xfrom = board.squareAttackedByPieceType(to,
                            board.getSideToMove(), fromPiece);
                    if (location.length() > 0) {
                        if (Character.isDigit(location.charAt(0))) {
                            int irank = Integer.parseInt(location);
                            if (!(irank >= 1 && irank <= 8)) {
                                throw new MoveConversionException("Couldn't parse rank: " + location);
                            }
                            Rank rank = Rank.allRanks[irank - 1];
                            xfrom &= Bitboard.getRankbb(rank);
                        } else {
                            try {
                                File file = File.valueOf("FILE_" + location.toUpperCase());
                                xfrom &= Bitboard.getFilebb(file);
                            } catch (Exception e) {
                                throw new MoveConversionException("Couldn't parse file: " + location);
                            }
                        }
                    }
                    if (xfrom != 0L) {
                        if (!Bitboard.hasOnly1Bit(xfrom)) {
                            xfrom = findLegalSquares(board, to, promotion, xfrom);
                        }
                        int f = Bitboard.bitScanForward(xfrom);
                        if (f >= 0 && f <= 63) {
                            from = Square.squareAt(f);
                        }
                    }
                }
            }

        }
        if (from.equals(Square.NONE)) {
            throw new MoveConversionException("Couldn't parse 'from' square " + san + " to setup: " + board.getFen());
        }
        return new Move(from, to, promotion);
    }

    public static String encodeToSan(final Board board, Move move) throws MoveConversionException {
        EnumMap<Piece, String> notation = sanNotation;
        StringBuilder san = new StringBuilder();
        Piece piece = board.getPiece(move.getFrom());
        if (piece.getPieceType().equals(PieceType.KING)) {
            int delta = move.getTo().getFile().ordinal() -
                    move.getFrom().getFile().ordinal();
            if (Math.abs(delta) >= 2) { // is castle
                if (!board.doMove(move, true)) {
                    throw new MoveConversionException("Invalid move [" +
                            move.toString() + "] for current setup: " + board.getFen());
                }
                san.append(delta > 0 ? "O-O" : "O-O-O");
                addCheckFlag(board, san);
                return san.toString();
            }
        }
        boolean pawnMove = piece.getPieceType().equals(PieceType.PAWN) &&
                move.getFrom().getFile().equals(move.getTo().getFile());
        boolean ambResolved = false;
        san.append(notation.get(piece));
        if (!pawnMove) {
            //resolving ambiguous move
            long amb = board.squareAttackedByPieceType(move.getTo(),
                    board.getSideToMove(), piece.getPieceType());
            amb &= ~move.getFrom().getBitboard();
            if (amb != 0L) {
                List<Square> fromList = Bitboard.bbToSquareList(amb);
                for (Square from : fromList) {
                    if (!board.isMoveLegal(new Move(from, move.getTo()), false)) {
                        amb ^= from.getBitboard();
                    }
                }
            }
            if (amb != 0L) {
                if ((Bitboard.getFilebb(move.getFrom()) & amb) == 0L) {
                    san.append(move.getFrom().getFile().getNotation().toLowerCase());
                } else if ((Bitboard.getRankbb(move.getFrom()) & amb) == 0L) {
                    san.append(move.getFrom().getRank().getNotation().toLowerCase());
                } else {
                    san.append(move.getFrom().toString().toLowerCase());
                }
                ambResolved = true;
            }
        }

        if (!board.doMove(move, true)) {
            throw new MoveConversionException("Invalid move [" +
                    move.toString() + "] for current setup: " + board.getFen());
        }


        Piece captured = board.getBackup().getLast().getCapturedPiece();
        boolean isCapture = !captured.equals(Piece.NONE);
        if (isCapture) {
            if (!ambResolved &&
                    piece.getPieceType().equals(PieceType.PAWN)) {
                san.append(move.getFrom().getFile().getNotation().toLowerCase());
            }
            san.append("x");
        }
        san.append(move.getTo().toString().toLowerCase());
        if (!move.getPromotion().equals(Piece.NONE)) {
            san.append("=");
            san.append(notation.get(move.getPromotion()));
        }
        addCheckFlag(board, san);
        return san.toString();
    }

    private static void addCheckFlag(Board board, StringBuilder san) {
        if (board.isKingAttacked()) {
            if (board.isMated()) {
                san.append("#");
            } else {
                san.append("+");
            }
        }
    }

    private static long findLegalSquares(Board board, Square to, Piece promotion, long pieces) {
        long result = 0L;

        if (pieces != 0L) {
            for (Square sqSource : Bitboard.bbToSquareList(pieces)) {
                Move move = new Move(sqSource, to, promotion);
                if (board.isMoveLegal(move, true)) {
                    result |= sqSource.getBitboard();
                    break;
                }
            }
        }

        return result;
    }

    /** Forma de normalizar un string SAN eliminando la información no imprescindible, como los ? / !, que se usan
     * comumente si un movimiento se considera un error o imprecisión */
    public static String normalizeSan(String san) {
        return san.replace("+", "")
                .replace("#", "")
                .replace("!", "")
                .replace("?", "")
                .replace("ep", "")
                .replace("\n", " ");
    }
}
