# 📚♟️ Chesslearn  - Proyecto fin de ciclo

Página web especializada en el aprendizaje del ajedrez, tanto para principantes como para maestros. Dispone de una aplicación Android.

![web1](doc/img/web3.png)

## Prueba la aplicación

### Web

https://dcostap-chess.herokuapp.com/index.html

> Ten en cuenta que debido a las características del servidor web (el cual ha sido contratado de forma gratuita), la página puede tardar varios segundos en cargar si se accede por primera vez en mucho tiempo.

### Android

El `apk` correspondiente al _release_ más reciente de la aplicación se encuentra en: [`chesslearn_android/releases/chesslearn.apk`](chesslearn_android/releases/chesslearn.apk).

Dentro de la aplicación, desde la pantalla incial se puede acceder a éstas dos pantallas:
- Análisis de Aperturas: Donde puedes realizar movimientos en el tablero de ajedrez y ver la información de cada movimiento posible.
- Puzzles: Donde puedes poner a prueba tu conocimiento de tácticas resolviendo diversos puzzles.

## Puesta en marcha del código fuente

### Web

Ten en cuenta que se utiliza un `git submodule` para almacenar el repositorio de la web en la carpeta `web/`, pero éste es a su vez su propio [repositorio git](https://gitlab.iessanclemente.net/a19dariocp/chess-web-proyecto-fin-de-ciclo).

Después de haber clonado este repositorio, para descargar también los archivos del `submodule` dentro de `web/` ejecuta:

`git submodule update --init`

Revisa la información en el [repositorio git de la web](https://gitlab.iessanclemente.net/a19dariocp/chess-web-proyecto-fin-de-ciclo) para aprender más sobre su estructura y despliegue.

### Android

1. Descarga la última versión de [Android Studio](https://developer.android.com/studio).
2. Dentro de Android Studio, abre la carpeta del proyecto android (`chesslearn_android/`).
3. Espera a que el sistema de dependencias `gradle` termine de preparar el proyecto.
4. Puedes probar la aplicación creando un dispositivo emulado de Android (AVD - *Android Virtual Device*), o conectando por _usb_ tu dispositivo Android real. Para más información consulta los [_docs_ oficiales](https://developer.android.com/studio/run/emulator)

## Estructura del código

### Web

Toda la información está expuesta el [repositorio git de la web](https://gitlab.iessanclemente.net/a19dariocp/chess-web-proyecto-fin-de-ciclo).

### Android

- `core/src`
  - Contiene código que no tiene una relación directa con el SDK de Android.
  - `ChessDatabaseProcessor`: Clase que lanza un proceso que analiza posiciones partidas reales de un archivo en local. La información se guarda en una base de datos local, que es la que más tarde se traslada al servidor. Es la información sobre los posibles movimientos en una posicón usada en la pantalla *Análisis de Aperturas*.
  - `OpeningsDatabaseProcessor`: Clase que lanza un proceso que extraye todos los nombres de aperturas de un archivo online. Esta información se añade a la base de datos, la misma que en `ChessDatabaseProcessor`.
  - `ChessGame`: Es la clase que se encarga de dibujar el tablero de ajedrez y procesar la interacción con el usuario. Ha sido programado todo a mano a partir de la librería de gráficos *libgdx*.


- `android/src`
  - Contiene código que tiene una relación directa con el SDK de Android. Las clases principales son:
    - `ActivityGame`: La pantalla de *Análisis de Aperturas*.
    - `ActivityPuzzle`: La pantalla de Puzzles.
    - `CustomListViewAdapter`: Define un ListView personalizado que es el usado para mostrar la lista de movimientos posibles en el *Análisis de Aperturas*.

## Anteproyecto & Diseño

1. Anteproyecto
    * 1.1. [Contextualización](doc/contextualizacion.md)
    * 1.2. [Análisis](doc/analisis.md)
    * 1.3. [Necesidades](doc/necesidades.md)
    * 1.4. [Tecnologías](doc/tecnologias.md)
2. [Diseño](doc/diseño.md)

## Sobre el autor

Hola! 👋 Me llamo Darío Costa Pena y en este momento estoy a punto de finalizar el ciclo de Desarrollo de Aplicaciones Multiplataforma.

Se trata de un ciclo que siempre he querido hacer ya que llevo realizando proyectos de programación por mi cuenta desde pequeño, y necesitaba conseguir este título para poder introducirme de lleno en el mundo laboral.

He escrito programas en muchos lenguajes de programación y entornos, pero nunca en el campo de las tecnologías *web*; razón por la cual me he decantado por este proyecto. He aprendido mucho y ha resultado ser un reto muy divertido.

No dudes en contactar conmigo a través del correo dariocostapena@gmail.com.

## Guía de contribución

Cualquier tipo de contribución se agradece. Para aportar más código o funcionalidades, clona / haz un *fork* del proyecto y crea una _pull request_ para que pueda revisar los cambios o mejoras realizados.

También puedes informar de errores mediante la pestaña de _Issues_ en Gitlab.

## Licencia

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0)
